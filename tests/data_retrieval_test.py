"'Here are some tests for boris part'"

import unittest
from datetime import datetime
from miner_src.preprocess import api_setup, get_objects_from_elsapy
from miner_src.data_retrieval import PaperAuthor, PaperData, extract_fieldset, extract_citations


def object_helper(paper_name):
    "'Basic helper for establishing the client and fetching the dict from elsapy"
    title = [paper_name]
    client = api_setup()
    obj = get_objects_from_elsapy(client, title)[0][0]
    return client, obj


class DataRetrievalTestCases(unittest.TestCase):
    "'Class to store the test cases for the data retrival module"

    def test_fields_extracted_basic(self):
        "'check the correct fields are extracted for a sample paper"
        client, elsapy_dict = object_helper(
            'GHUMVEE - Efficient, effective, and flexible replication')
        obj = extract_fieldset(client, elsapy_dict)

        # Question - how do we test when the data is constantly changing? Are these tests
        # just too specific?
        # Ans: To an extent, we should def build out a better method of testing that allows quick
        # swapping of datasets (and thus better validation)
        authors_groundtruth = [PaperAuthor('Volckaert, S.', 1, 41),
                               PaperAuthor('De Sutter, B.', 2, 1076),
                               PaperAuthor('De Baets, T.', 3, 3),
                               PaperAuthor('De Bosschere, K.', 4, 2462)]
        obj_groundtruth = PaperData()
        obj_groundtruth.paper_id = 84875925779
        obj_groundtruth.title = 'GHUMVEE: Efficient, effective, and flexible replication'
        obj_groundtruth.num_citations = 3
        obj_groundtruth.num_downloads = 0
        obj_groundtruth.publication_date = datetime(2013, 4, 12)
        obj_groundtruth.journal_name = 'Lecture Notes in Computer Science (including ' \
        + 'subseries Lecture Notes in Artificial Intelligence and Lecture Notes in ' \
        + 'Bioinformatics)'
        obj_groundtruth.journal_impact_factor = 0.655
        obj_groundtruth.abstract = ''
        obj_groundtruth.authors = authors_groundtruth
        obj_groundtruth.citations = []
        obj_groundtruth.references = []

        self.assertEqual(obj.paper_id, obj_groundtruth.paper_id)
        self.assertEqual(obj.title, obj_groundtruth.title)
        self.assertEqual(obj.num_citations, obj_groundtruth.num_citations)
        self.assertEqual(obj.publication_date,
                         obj_groundtruth.publication_date)
        self.assertEqual(obj.journal_name, obj_groundtruth.journal_name)
        self.assertEqual(obj.journal_impact_factor, obj_groundtruth.journal_impact_factor)
        # NOT IMPLEMENTED
        # self.assertEqual(obj.num_downloads, obj_groundtruth.num_downloads) # NOT IMPLEMENTED

        # self.assertEqual(obj.abstract, obj_groundtruth.abstract)

    # def test_fields_extracted_authors(self):
    #     "'Check that authors are extracted correctly"
    #     return None

    def test_fields_extracted_citations(self):
        "'Check that fields are extracted correctly"
        title1 = 'Taming parallelism in a multi-variant execution environment'
        title2 = 'Cloning your gadgets: Complete ROP attack immunity with multi-variant execution'
        title3 = 'Double helix and RAVEN: A system for cyber fault tolerance and recovery'
        obj_groundtruth = [{'id': '85019261284', 'title': title1},
                           {'id': '84978652278', 'title': title2},
                           {'id': '84968677677', 'title': title3}]
        client, elsapy_dict = object_helper(
            'GHUMVEE - Efficient, effective, and flexible replication')
        citations = extract_citations('84875925779', client, elsapy_dict)
        for citation, groundtruth in zip(citations, obj_groundtruth):
            self.assertEqual(citation["id"], groundtruth["id"])
            self.assertEqual(citation["title"], groundtruth["title"])

    # def test_fields_extracted_references(self):
    #     "'Check that citations are extracted correctly"
    #     return None

    # def test_fields_missing_fields(self):
    #     "'Check that a paper with missing fields is handled correctly"
    #     return None

    # def test_fields_extracted_zero_lists(self):
    #     "'Check that a paper with no authors/citations/references is handled correctly"
    #     return None

if __name__ == '__main__':
    unittest.main()
