"Tests for preprocessing data"
import unittest
import xlrd
from miner_src.preprocess import api_setup, get_all_titles,\
    get_objects_from_elsapy, compare_two_titles, search_one_object


class PreprocessingTestCases(unittest.TestCase):
    "'Class to store the test cases for the preprocessing module"
    def test_number_of_titles(self):
        "'check if all titles are retrieved from the excel file'"
        path = 'test_data/test.xlsx'
        work_book = xlrd.open_workbook(path)  # Load the excel workbook
        sheet = work_book.sheet_by_index(0)
        self.assertEqual(len(get_all_titles(path)), sheet.nrows)

    def test_number_of_objects(self):
        "'check if all objects are extracted by their titles"
        path = 'test_data/test.xlsx'
        titles = get_all_titles(path)
        client = api_setup()
        obj, _ = get_objects_from_elsapy(client, titles)
        self.assertEqual(len(obj), len(titles))

    def test_title_with_special_characters(self):
        "'check if the program can handle titles with special characters'"
        client = api_setup()
        title_with_brackets_1 = \
            "Secure SOurce-BAsed Loose Synchronization (SOBAS) for wireless sensor networks"
        result_1 = search_one_object(client, title_with_brackets_1)
        self.assertTrue(compare_two_titles(title_with_brackets_1, result_1["dc:title"]))
        title_with_brackets_2 = "A pattern recognition scheme for Distributed Denial of " \
                                "Service (DDoS) attacks in wireless sensor networks"
        result_2 = search_one_object(client, title_with_brackets_2)
        self.assertTrue(compare_two_titles(title_with_brackets_2, result_2["dc:title"]))
        title_with_ampersand_1 = "Joint scheduling & jamming for data secrecy in wireless networks"
        result_3 = search_one_object(client, title_with_ampersand_1)
        self.assertTrue(compare_two_titles(title_with_ampersand_1, result_3["dc:title"]))
        title_with_ampersand_2 = \
            "Design guidelines for security protocols to prevent replay & parallel session attacks"
        result_4 = search_one_object(client, title_with_ampersand_2)
        self.assertTrue(compare_two_titles(title_with_ampersand_2, result_4["dc:title"]))

    def test_multiple_results_for_one_title(self):
        "'check if the program returns one result only with the correct title'"
        client = api_setup()
        title = "Exposing and eliminating vulnerabilities to denial" \
                " of service attacks in secure gossip-based multicast"
        result = search_one_object(client, title)
        self.assertTrue(compare_two_titles(result['dc:title'], title))

    def test_all_titles_same_as_input(self):
        '"Check if the retrieved titles are same as the input ones"'
        # this test was built on the premise that all input article titles are correct and complete
        path = 'test_data/test.xlsx'
        titles = get_all_titles(path)
        client = api_setup()
        results, _ = get_objects_from_elsapy(client, titles)
        for i, title in enumerate(titles, 0):
            self.assertTrue(compare_two_titles(title, results[i]['dc:title']))


if __name__ == '__main__':
    unittest.main()
