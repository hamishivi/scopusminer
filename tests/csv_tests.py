"Tests for csv conversion"

import unittest
import csv
from miner_src.obj_to_json import to_csv

class CSVConversionTestCases(unittest.TestCase):
    "'Class to store the test cases for the to_csv method"

    def test_basic_csv(self):
        "'check the method operates as expected"
        obj = {"a":1, "b":"2", "c":3, "complex": {"a":1}, "list": [1, 2, 3]}

        to_csv([obj], "out.csv")
        with open("out.csv") as f:
            reader = csv.reader(f)
            self.assertEqual(["a", "b", "c", "complex", "list"], next(reader))
            self.assertEqual(["1", "2", "3", "{'a': 1}", "[1, 2, 3]"], next(reader))
