"""test cases for papers and authors data structures"""
import json
from miner_src.data_structure import PaperData
from miner_src.data_structure import PaperAuthor
from miner_src.obj_to_json import to_json

def make_author():
    """makes dummy author"""
    return PaperAuthor("author1", 1, 100)

def make_paper():
    """makes dummy paper"""
    author1 = PaperAuthor("author1", 1, 100)
    author2 = PaperAuthor("author2", 2, 100)
    author3 = PaperAuthor("author3", 3, 100)

    authors = [author1, author2, author3]

    paper = PaperData(123, "test paper", 3, 1000, 1534651900,
                      "test journal", 100, "test abstract", authors)

    return paper

def test_author_class():
    ''' test author class'''
    test_author = make_author()
    assert test_author.author_name == 'author1'
    assert test_author.sequence_number == 1
    assert test_author.author_citations == 100

def test_paper_class():
    '''test paper class'''

    test_paper = make_paper()

    assert test_paper.id == 123
    assert test_paper.title == "test paper"
    assert test_paper.num_citations == 3
    assert test_paper.num_downloads == 1000
    assert test_paper.publication_date == 1534651900
    assert test_paper.journal_name == "test journal"
    assert test_paper.journal_impact_factor == 100
    assert test_paper.abstract == "test abstract"
    assert test_paper.authors[0].author_name == 'author1'

def test_json_conversion():
    """converts json inti python dict, and tests"""
    test_paper = make_paper()
    test_json_string = to_json(test_paper)
    test_json = json.loads(test_json_string)

    assert test_json['id'] == 123
    assert test_json['title'] == "test paper"
    assert test_json['num_citations'] == 3
    assert test_json['num_downloads'] == 1000
    assert test_json['publication_date'] == 1534651900
    assert test_json['journal_name'] == "test journal"
    assert test_json['journal_impact_factor'] == 100
    assert test_json['abstract'] == "test abstract"
    assert test_json['authors'][0]['author_name'] == 'author1'
