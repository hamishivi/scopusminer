"""
Script to populate neo4j with papers and hsbm topics
"""

from py2neo import Graph, Node, Relationship
import pandas as pd

df_h = pd.read_csv('../../data/hsbm.csv')
df_h = df_h.rename(columns={'title': 'paper_id'})
df_f = pd.read_csv('../../data/file1-sentiments.csv')

df = pd.merge(df_f, df_h, how='inner', on='paper_id')

df = df.dropna()#df['subjects'] = df['subjects'].apply(lambda x: x[0])


graph = Graph(password='Scopus')


i = 0
for index, row in df.iterrows():
    paper = Node("paper", paper_id=row['paper_id'], title=row['title'], publication_date=row['publication_date'],
    abstract=row['abstract'], sentiment=row['sentiment_category_binary'])

    hsbm_cluster = Node("hsbm_cluster", cluster_number=row['cluster_no'])

    rel = Relationship(paper, "CLUSTERED_IN", hsbm_cluster)
    tx = graph.begin()
    tx.merge(paper, "paper", "paper_id") #node,label,primary key
    tx.merge(hsbm_cluster, "hsbm_cluster", "cluster_number") #node,label,pirmary key
    tx.merge(rel)
    tx.commit()
    print((i/len(df)*100), '%')
    i += 1
