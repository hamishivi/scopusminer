"""
Script to populate neo4j with papers and keywords
"""

from py2neo import Graph, Node, Relationship
import pandas as pd

df = pd.read_csv('../../data/title-keyword-summa.csv')
df = df.dropna()
df['kw_list'] =  df.Keywords.apply(lambda x: x.split(','))
#df['subjects'] = df['subjects'].apply(lambda x: x[0])

graph = Graph(password='Scopus')


i = 0
for index, row in df.iterrows():
    paper = Node("paper", paper_id=row['Paper_id'])
    #graph.create(paper)


    for kw in row['kw_list']:
        kw = Node("key_word", word=kw)
    #graph.create(subject)
        rel = Relationship(paper, "CONTAINS", kw)
        tx = graph.begin()
        tx.merge(paper, "paper", "paper_id") #node,label,primary key
        tx.merge(kw, "key_word","word") #node,label,pirmary key
        tx.merge(rel)
        tx.commit()



    print((i/len(df)*100), '%')
    i += 1
