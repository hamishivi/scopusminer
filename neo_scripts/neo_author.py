"""
Script to propulate neo4j with authors and papers
"""

from py2neo import Graph, Node, Relationship
import pandas as pd

df = pd.read_csv('../../data/full_set_02.csv')
df = df.dropna()#df['subjects'] = df['subjects'].apply(lambda x: x[0])

graph = Graph(password='Scopus')


i = 0
for index, row in df.iterrows():
    paper = Node("paper", paper_id=row['paper_id'])
    author = Node("author", name=row['name'], num_citations=row['num_citations'])

    rel = Relationship(author, "WROTE", paper)
    tx = graph.begin()
    tx.merge(paper, "paper", "paper_id") #node,label,primary key
    tx.merge(author, "author", "name") #node,label,pirmary key
    tx.merge(rel)
    tx.commit()
    print((i / len(df) * 100), '%')
    i += 1

#graph.schema.create_uniqueness_constraint('subject','subject')
    #graph.merge('subject')
