"""
Script to populate neo4j with papers and their references

This script is commented out, becuase it creates circular references in the database,
Since reference is the reverse relationship of citation.

I have left the code here as a comment for completeness


from py2neo import Graph, Node, Relationship
import pandas as pd

df = pd.read_csv('../../data/full_set_03.csv')
df = df.dropna()#df['subjects'] = df['subjects'].apply(lambda x: x[0])
#df = df.loc[df['is_paper'] == True]

graph = Graph(password='Scopus')


i = 0
for index, row in df.iterrows():
    paper = Node("paper", paper_id=row['paper_id'])
    citation = Node("paper", paper_id=row['id'])

    rel = Relationship(paper, "REFERENCED_BY", citation)
    tx = graph.begin()
    tx.merge(paper, "paper", "paper_id") #node,label,primary key
    tx.merge(citation, "paper", "paper_id") #node,label,pirmary key
    tx.merge(rel)
    tx.commit()
    print((i / len(df) * 100), '%')
    i += 1

#graph.schema.create_uniqueness_constraint('subject','subject')
    #graph.merge('subject')
"""
