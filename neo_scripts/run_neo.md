# Neo4j Scripts

These are the scripts to run populate the neo4j db, if you are working with the repo

1. Install the neo4j desktop app
2. Create a new project, and have the browser window open, so you are in the GUI
3. run the ```neo.sh``` script 

```neo.sh``` runs the python files in this directory, so ensure they have the correct file names and paths to the extracted csv datasets.

To run the tests, you will need the browser window open like above, and please set the database password to 'Scopus' (this can be done in the administration tab in the desktop app). Also ensure you have installed the requirements listed in ```requirements.txt```. You can then run the tests with:

```pytest test_neo.py```

We have run the tests locally and they do pass.