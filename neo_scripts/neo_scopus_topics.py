"""
Script to populate neo4j with papers and scopus topics
"""

from py2neo import Graph, Node, Relationship
import pandas as pd
import ast

df = pd.read_csv('../../data/full_set_01.csv')
df = df.dropna()
df['subjects'] = df['subjects'].apply(ast.literal_eval)
#df['subjects'] = df['subjects'].apply(lambda x: x[0])

graph = Graph(password='Scopus')


i = 0
for index, row in df.iterrows():
    paper = Node("paper", paper_id=row['paper_id'],title=row['title'], publication_date=row['publication_date'], abstract=row['abstract'])
    #graph.create(paper)


    for subject in row['subjects']:
        subject = Node("scopus_subject", subject_name=subject)
    #graph.create(subject)
        rel = Relationship(paper, "ABOUT", subject)
        tx = graph.begin()
        tx.merge(paper, "paper", "paper_id") #node,label,primary key
        tx.merge(subject, "scopus_subject", "subject_name") #node,label,pirmary key
        tx.merge(rel)
        tx.commit()

    print((i/len(df)*100), '%')
    i += 1

#graph.schema.create_uniqueness_constraint('subject','subject')
    #graph.merge('subject')
