"""
Script to populate neo4j with papers and their references
"""

from py2neo import Graph, Node, Relationship
import pandas as pd

df = pd.read_csv('../../data/full_set_04.csv')
df = df.loc[df['is_paper']]
df = df.dropna()#df['subjects'] = df['subjects'].apply(lambda x: x[0])
#df = df.loc[df['is_paper'] == True]

graph = Graph(password='Scopus')


i = 0
for index, row in df.iterrows():
    paper = Node("paper", paper_id=row['paper_id'])
    reference = Node("paper", paper_id=row['ref_id'], title=row['ref_title'])

    rel = Relationship(paper, "REFERENCES", reference)
    tx = graph.begin()
    tx.merge(paper, "paper", "paper_id") #node,label,primary key
    tx.merge(reference, "paper", "paper_id") #node,label,pirmary key
    tx.merge(rel)
    tx.commit()
    print((i / len(df) * 100), '%')
    i += 1

#graph.schema.create_uniqueness_constraint('subject','subject')
    #graph.merge('subject')
