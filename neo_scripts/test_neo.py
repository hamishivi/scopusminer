"""
Neo4j Test cases, run with browser app open.
Assumes using our final dataset, available in our repo.
"""
from py2neo import Graph, NodeMatcher, RelationshipMatcher

from py2neo.database import Schema

GRAPH = Graph(password='Scopus')
MATCHER = NodeMatcher(GRAPH)
RMAT = RelationshipMatcher(GRAPH)
#cypher = GRAPH.cypher

node = MATCHER.match("journal",
                     title='IEEE International Conference on Communications')
rel = RMAT.match(set(node), r_type='PUBLISHED').where('_.paper_id==84871989307')

def test_labels():
    "tests all labels are present"
    topics = ['journal', 'lda_cluster', 'key_word',
              'hsbm_cluster', 'paper', 'author', 'scopus_subject']
    assert sorted(list(Schema(GRAPH).node_labels)) == sorted(topics)


def test_rels():
    "tests all relationships are present"
    rels = ['CONTAINS', 'PUBLISHED', 'WROTE', 'REFERENCES', 'CLUSTERED_IN', 'ABOUT']
    assert sorted(list(Schema(GRAPH).relationship_types)) == sorted(rels)


def test_paper():
    "test an arbirary paper"
    node = MATCHER.match("paper", paper_id=84930024739).first()
    assert node['title'] == 'Use of Tsallis entropy in detection of SYN flood DoS attacks'

def test_wote():
    "test wrote relationship"
    node = MATCHER.match("paper", paper_id=84930024739)
    rel = RMAT.match(set(node), r_type='WROTE').where('_.name==Popovic M.')
    assert rel is not None


def test_pub():
    "test journal and published relationship"
    node = MATCHER.match("journal",
                         title='IEEE International Conference on Communications')
    rel = RMAT.match(set(node), r_type='PUBLISHED').where('_.paper_id==84871989307')
    assert rel is not None

def test_write_count():
    "test top author"
    node = MATCHER.match("author", name='Zhou W.')
    rel = RMAT.match(set(node), 'WROTE')
    assert len(rel) == 21

def key_word_count():
    "test top keyword"
    node = MATCHER.match("key_word", word='attack')
    rel = RMAT.match(set(node))
    assert len(rel) == 405

def test_node_counts():
    "test all nodes are present"
    #test authors
    assert GRAPH.evaluate("MATCH (n:author) RETURN count(n)") == 2802

    #test papers
    assert GRAPH.evaluate("MATCH (n:paper) RETURN count(n)") == 13659

    #test journal
    assert GRAPH.evaluate("MATCH (n:journal) RETURN count(n)") == 661

    #test scopus subjects
    assert GRAPH.evaluate("MATCH (n:scopus_subject) RETURN count(n)") == 83

    #test key_word
    assert GRAPH.evaluate("MATCH (n:key_word) RETURN count(n)") == 3989

    #hsbm cluster
    assert GRAPH.evaluate("MATCH (n:hsbm_cluster) RETURN count(n)") == 18

    #lda CLUSTERED_IN
    assert GRAPH.evaluate("MATCH (n:lda_cluster) RETURN count(n)") == 231

    #total count
    assert GRAPH.evaluate("MATCH (n) RETURN count(n)") == 21443


def test_rel_counts():
    "test relationship counts"

    #clustered in count
    assert GRAPH.evaluate("MATCH p=()-[r:CLUSTERED_IN]->() RETURN count(r)") == 4173

    #about count
    assert GRAPH.evaluate("MATCH p=()-[r:ABOUT]->() RETURN count(r)") == 2881

    #CONTAIS count
    assert GRAPH.evaluate("MATCH p=()-[r:CONTAINS]->() RETURN count(r)") == 11652

    #published count
    assert GRAPH.evaluate("MATCH p=()-[r:PUBLISHED]->() RETURN count(r)") == 1301

    #References
    assert GRAPH.evaluate("MATCH p=()-[r:REFERENCES]->() RETURN count(r)") == 22323

    #WROTE
    assert GRAPH.evaluate("MATCH p=()-[r:WROTE]->() RETURN count(r)") == 4290

    #total relationship
    assert GRAPH.evaluate("MATCH p=()-->() RETURN count(p)") == 46620
