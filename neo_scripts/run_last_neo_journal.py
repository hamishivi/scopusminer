"""
Script to populate neo4j with papers and journals
"""
from py2neo import Graph, Node, Relationship
import pandas as pd

df = pd.read_csv('../../data/file1-sentiments.csv')
df = df.dropna()#df['subjects'] = df['subjects'].apply(lambda x: x[0])

graph = Graph(password='Scopus')


i = 0
for index, row in df.iterrows():
    paper = Node("paper", paper_id=row['paper_id'],title=row['title'], publication_date=row['publication_date'],
    abstract=row['abstract'], sentiment=row['sentiment_category_binary'])

    journal = Node("journal", name=row['journal_name'], impact_factor=row['journal_impact_factor'])

    rel = Relationship(journal, "PUBLISHED", paper)
    tx = graph.begin()
    tx.merge(paper, "paper", "paper_id") #node,label,primary key
    tx.merge(journal, "journal", "name") #node,label,pirmary key
    tx.merge(rel)
    tx.commit()
    print('\r',(i/len(df)*100), '%')
    i += 1

#graph.schema.create_uniqueness_constraint('subject','subject')
    #graph.merge('subject')
