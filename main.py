"The starting point for the program"
import argparse
import time
from multiprocessing import Pool
from functools import partial

from miner_src.preprocess import get_all_titles, api_setup, get_objects_from_elsapy
from miner_src.data_retrieval import extract_fieldset
from miner_src.obj_to_json import to_json, to_csv

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("input_file")
    parser.add_argument("output_file")
    parser.add_argument("-b", "--batch", default=10)
    parser.add_argument("-v", "--verbose", action='store_true')
    parser.add_argument("-o", "--offset", default=0)
    args = parser.parse_args()

    input_file = args.input_file
    output_file = args.output_file
    batch = int(args.batch)
    verbose = args.verbose
    offset = args.offset

    titles = get_all_titles(input_file)[int(offset):]
    client = api_setup()

    # Split up the titles into individual batches
    batch_size = min(batch, len(titles))
    num_batches = int(len(titles)/batch_size)
    title_batches = [titles[i*batch_size:(i+1)*batch_size]
                     for i in range(num_batches)]
    if len(titles) % batch_size != 0:
        title_batches.append(titles[num_batches*batch_size:])

    # Perform fetching batch-by-batch
    for batch_index, _ in enumerate(title_batches):
        if verbose:
            print("--- Batch {}/{}".format(batch_index, len(title_batches)))
            print("Starting Object extraction from elsapy")
            start = time.perf_counter()

        elsapy_dicts, missing_titles = get_objects_from_elsapy(
            client, title_batches[batch_index])

        if verbose:
            print("{:.4f}s elapsed for object extraction.\nStarting fieldset extraction".format(
                time.perf_counter()-start))
            start = time.perf_counter()

        agents = 5
        chunksize = 3
        with Pool(processes=agents) as pool:
            python_obj = pool.map(
                partial(extract_fieldset, client), elsapy_dicts, chunksize)
        #python_obj = [extract_fieldset(client, object) for object in elsapy_dicts]

        # Remove null values that appear as a result of failed extraction
        python_obj = [x for x in python_obj if x is not None]
        json = [to_json(obj) for obj in python_obj]

        if verbose:
            print("{:.4f}s elapsed for fieldset extraction.\nExporting data".format(
                time.perf_counter()-start))
            start = time.perf_counter()

        if output_file.endswith('.csv'):
            dicts = [obj.to_dict() for obj in python_obj]
            sheet1dicts = []
            sheet2dicts = []
            sheet3dicts = []
            sheet4dicts = []
            out_name = output_file[:-4]
            for d in dicts:
                sheet1dicts.append({k: d[k] for k in ("paper_id", "title", "num_citations",
                                                      "publication_date", "journal_name",
                                                      "journal_impact_factor",
                                                      "abstract", "subjects") if k in d})
                authors = d["authors"]
                for author in authors:
                    sheet2dicts.append(author)
                citations = d["citations"]
                for citation in citations:
                    sheet3dicts.append(citation)
                references = d["references"]
                for reference in references:
                    sheet4dicts.append(reference)

            to_csv(sheet1dicts, "{}-{:02d}_1.csv".format(out_name, batch_index))
            to_csv(sheet2dicts, "{}-{:02d}_2.csv".format(out_name, batch_index))
            to_csv(sheet3dicts, "{}-{:02d}_3.csv".format(out_name, batch_index))
            to_csv(sheet4dicts, "{}-{:02d}_4.csv".format(out_name, batch_index))

            # Append missing CSVs
            with open("{}-missing.csv".format(out_name), "a") as f:
                for i in missing_titles:
                    f.write("%s\n" % i)

        else:
            with open(output_file, 'w') as f:
                f.write(str(json))

    print("Process complete!")
    if verbose:
        print("{:.4f}s elapsed for export".format(time.perf_counter()-start))
