import re
import pandas as pd
import json

# the provided library for hsbm
from hSBM_Topicmodel.sbmtm import sbmtm
import graph_tool.all as gt

# other language tools
import gensim

# load data
df = pd.read_csv('../../data/week06-1.csv')
df = df.dropna()
df['abstract'] = df['abstract'].astype(str)
df['title'] = df['title'].astype(str)
# turn this df into a list of strings, fancy majick stuff
df['subjects'] = df['subjects'].apply(lambda x: re.sub('[^A-Za-z,\- ]', '', x).split(', '))
df['abstract'] = df['abstract'].apply(lambda x: re.sub(r'[\xc2\xa9][^\.]*\.', ' ', x))

# convert to sentences
def sent_to_words(sentences):
    for sentence in sentences:
        yield(gensim.utils.simple_preprocess(str(sentence), deacc=True))  # deacc=True removes punctuations

# now we create the JSON objects that the fishbone JS code wants.
fishbones = []
subjects_seen = {}

# group by subject. Can this be optimised?
for row in df.itertuples():
    for subject in row.subjects:
        if subject not in subjects_seen:
            subjects_seen[subject] = []
        subjects_seen[subject].append((row.title, row.abstract))

for subject in subjects_seen:
    print('fitting topic', subject, "-- titles:", len(subjects_seen[subject]))
    titles = [r[0] for r in subjects_seen[subject]]
    abstracts = [r[1] for r in subjects_seen[subject]]
    data_words = list(sent_to_words(abstracts))
    # plug into the model
    model = sbmtm()
    model.make_graph(data_words, documents=titles)
    gt.seed_rng(32) ## seed for graph-tool's random number generator --> same results
    model.fit()
    # get model clusters. fallback to 0 cluster (everything in one cluster) if need be
    children = []
    try:
        clusters = model.clusters(l=1, n=5)
    except KeyError:
        print(subject, 'didn\'t have enough titles for us to cluster/no clusters detected. Instead, we just list everything.')
        clusters = model.clusters(l=0, n=5)
    for k in clusters:
        children.append({"name": k, "children": [{"name": p[0]} for p in clusters[k]]})
    fishbones.append({"name": subject, "children": children})

# dump into data files
for fishbone in fishbones:
    str_bone = json.dumps(fishbone)
    with open('fishbone/hsbm_json/fishbone_' + fishbone["name"].replace(" ", "_") + '.json', 'w') as f:
        f.write(str_bone)

print("done")
