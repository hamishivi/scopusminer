import re
import pandas as pd
import json

# Gensim
import gensim
import gensim.corpora as corpora
from gensim.utils import simple_preprocess

# spacy for lemmatization
import spacy

# tuning variable
num_topics = 20

# do our setup and cleaning
df = pd.read_csv('../../data/week06-1.csv')
df = df.dropna()
df['abstract'] = df['abstract'].astype(str)
df['title'] = df['title'].astype(str)
df['subjects'] = df['subjects'].apply(lambda x: re.sub('[^A-Za-z,\- ]', '', x).split(', '))
df['abstract'] = df['abstract'].apply(lambda x: re.sub(r'[\xc2\xa9][^\.]*\.', ' ', x))

data_words = df.abstract.values.tolist()

# NLTK Stop words setup
from nltk.corpus import stopwords
stop_words = stopwords.words('english')
stop_words.extend(['from', 'subject', 're', 'edu', 'use'])

def sent_to_words(sentences):
    for sentence in sentences:
        yield(gensim.utils.simple_preprocess(str(sentence), deacc=True))  # deacc=True removes punctuations

# Build the bigram and trigram models
bigram = gensim.models.Phrases(data_words, min_count=5, threshold=100) # higher threshold fewer phrases.
trigram = gensim.models.Phrases(bigram[data_words], threshold=100)  

# Faster way to get a sentence clubbed as a trigram/bigram
bigram_mod = gensim.models.phrases.Phraser(bigram)
trigram_mod = gensim.models.phrases.Phraser(trigram)

# Define functions for stopwords, bigrams, trigrams and lemmatization
def remove_stopwords(texts):
    return [[word for word in simple_preprocess(str(doc)) if word not in stop_words] for doc in texts]

def make_bigrams(texts):
    return [bigram_mod[doc] for doc in texts]

def make_trigrams(texts):
    return [trigram_mod[bigram_mod[doc]] for doc in texts]

def lemmatization(texts, allowed_postags=['NOUN', 'ADJ', 'VERB', 'ADV']):
    """https://spacy.io/api/annotation"""
    texts_out = []
    for sent in texts:
        doc = nlp(" ".join(sent)) 
        texts_out.append([token.lemma_ for token in doc if token.pos_ in allowed_postags])
    return texts_out

# Initialize spacy 'en' model, keeping only tagger component (for efficiency)
# python3 -m spacy download en
nlp = spacy.load('en', disable=['parser', 'ner'])


# now we create the JSON objects that the fishbone JS code wants.
fishbones = []
subjects_seen = {}

# group by subject. Can this be optimised?
for row in df.itertuples():
    for subject in row.subjects:
        if subject not in subjects_seen:
            subjects_seen[subject] = []
        subjects_seen[subject].append((row.title, row.abstract))

for subject in subjects_seen:
    print('fitting topic', subject, "-- titles:", len(subjects_seen[subject]))
    # grab data
    titles = [r[0] for r in subjects_seen[subject]]
    abstracts = [r[1] for r in subjects_seen[subject]]
    data_words = list(sent_to_words(abstracts))
    # clean text
    data_words_nostops = remove_stopwords(data_words)
    data_words_bigrams = make_bigrams(data_words_nostops)
    data_lemmatized = lemmatization(data_words_bigrams, allowed_postags=['NOUN', 'ADJ', 'VERB', 'ADV'])
    id2word = corpora.Dictionary(data_lemmatized)
    texts = data_lemmatized
    corpus = [id2word.doc2bow(text) for text in texts]
    # make model!
    lda_model = gensim.models.ldamodel.LdaModel(corpus=corpus,
                                               id2word=id2word,
                                               num_topics=num_topics, 
                                               random_state=100,
                                               update_every=1,
                                               chunksize=100,
                                               minimum_probability=0,
                                               passes=10,
                                               alpha='auto',
                                               per_word_topics=True)
    # filter documents into topics
    topic_groups = [[] for _ in range(num_topics)]
    topic_names = [",".join([w[0] for w in lda_model.show_topic(x, 4)]) for x in range(num_topics)]
    children = []
    cnt = 0
    for doc in corpus:
        doc_topics, word_topics, phi_values = lda_model.get_document_topics(doc, per_word_topics=True)
        max_topic = doc_topics[0][1]
        for topic in doc_topics:
            if topic[1] > max_topic:
                max_topic = topic[0]
        topic_groups[int(max_topic)].append(titles[cnt])
        cnt += 1
    for k, g in enumerate(topic_groups):
        if len(g) > 0:
            children.append({"name": topic_names[k], "children": [{"name": s} for s in g]})
    fishbones.append({"name": subject, "children": children})

for fishbone in fishbones:
    str_bone = json.dumps(fishbone)
    with open('fishbone/lda_json/fishbone_' + fishbone["name"].replace(" ", "_") + '.json', 'w') as f:
        f.write(str_bone)