import re
import numpy as np
import pandas as pd
from pprint import pprint

# spacy for lemmatization
import spacy
import gensim
from gensim.utils import simple_preprocess

import matplotlib.pyplot as plt

def sent_extraction(data, df):
    "' takes a list of fields and a dataframe, and returns an updated dataframe with the sentiment analysis results appended. Assumes that the strings in 'data' line up with those in df."

    # Currently Using NLTK Vader Sentiment Analyzer: 
    #  Hutto, C.J. & Gilbert, E.E. (2014). VADER: A Parsimonious Rule-based Model for
    #  Sentiment Analysis of Social Media Text. Eighth International Conference on
    #  Weblogs and Social Media (ICWSM-14). Ann Arbor, MI, June 2014.

    from nltk.sentiment.vader import SentimentIntensityAnalyzer
    sid = SentimentIntensityAnalyzer()
    sentiments = []

    # Extract sentiment for each abstract
    for abstract in data:
        print(abstract)
        ss = sid.polarity_scores(abstract)
        sentiments.append(ss)
        for k in sorted(ss):
            print('\t {0}: {1}, '.format(k, ss[k]), end='')
        print('\n')

    # Append back to dataframe
    return df.assign(sen_compound = [x['compound'] for x in sentiments], 
                   sen_neg = [x['neg'] for x in sentiments],
                   sen_neu = [x['neu'] for x in sentiments],
                   sen_pos = [x['pos'] for x in sentiments])
                   #,sen_rawdata = data)