'''
Module for the keyword graph visualisation. Please note generating
the keyword graph can be very slow!
'''
import os
import pandas as pd
from summa import keywords as k
import nltk
from nltk.stem import WordNetLemmatizer
import plotly.graph_objs as go
from plotly.offline import plot
import networkx as nx

def preprocess(kws):
    '''
    lemmatizes keywords to remove redundancies.
    '''
    wordnet_lemmatizer = WordNetLemmatizer()
    kws = [wordnet_lemmatizer.lemmatize(k.lower()) for k in kws]
    kws = list(set(kws))
    return kws


def generate_keyword_graph(input_file, output_file, top_k=10):
    '''
    Generate a html keyword graph in output_file, based on the
    data in input_file. This uses plotly.
    '''
    if not os.path.exists(input_file):
        print('could not find', input_file)
        return
    nltk.download('wordnet')
    nltk.download('stopwords')

    from nltk.corpus import stopwords

    # Change the input file here
    dataframe = pd.read_csv(input_file)
    # Import and add stopwords
    stop_words = set(stopwords.words('english'))
    stop_words.add('like')
    stop_words.add('unlike')
    articles = []
    titles = dataframe.title
    shared_keywords = {}
    displayed_ids = dataframe.paper_id[0:top_k]
    displayed_abstracts = dataframe.abstract[0:top_k]
    # Read the abstracts and extract keywords from them
    for paper_id, abstract in zip(displayed_ids, displayed_abstracts):
        try:
            keywords = k.keywords(abstract)
        except ValueError:
            continue
        paper_id = int(paper_id)
        keywords = keywords.splitlines()
        keywords = preprocess(keywords)
        # Create a list of article objects that have title-to-keywords
        articles.append(paper_id)
        for keyword in keywords:
            if len(keyword) < 2 or keyword in stop_words:
                continue
            if keyword in shared_keywords:
                shared_keywords[keyword].append(paper_id)
            else:
                shared_keywords[keyword] = []
                shared_keywords[keyword].append(paper_id)

    # Generating node positions randomly
    # pos = {i: (random.gauss(0, 0.005), random.gauss(0, 0.005)) for i in range(len(articles))}
    G = nx.random_geometric_graph(len(articles), 0)
    pos = nx.get_node_attributes(G, 'pos')
    position = {}
    nid = {}
    for n in pos:
        x, y = pos[n]
        position[articles[n]] = (x, y)
        nid[articles[n]] = n

    # Generating edges
    for shared_keyword in shared_keywords:
        shared_titles = shared_keywords[shared_keyword]
        if len(shared_titles) > 1:
            for i in range(0, len(shared_titles)-1):
                for j in range(i+1, len(shared_titles)):
                    try:
                        data = G.get_edge_data(nid[shared_titles[i]], nid[shared_titles[j]])
                    except KeyError:
                        continue
                    if data is None:
                        G.add_edge(nid[shared_titles[i]], nid[shared_titles[j]],
                                   labels=shared_keyword)
                    else:
                        words = data['labels'].split(', ')
                        # if it is a variant of the existing words, we skip it
                        if any(shared_keyword[0:4] in w for w in words):
                            continue
                        G.add_edge(nid[shared_titles[i]], nid[shared_titles[j]],
                                   labels=data['labels']+", "+shared_keyword)

    # Rearranging now that edges/nodes are added
    # pos = nx.get_node_attributes(G, 'pos')
    pos = nx.single_source_shortest_path_length(G, 0)
    # pos = nx.kamada_kawai_layout(G)

    # Generating plotly parameters
    node_trace = go.Scatter(
        x=[],
        y=[],
        text=[],
        mode='markers',
        hoverinfo='text',
        marker=dict(
            showscale=True,
            # colorscale options
            #'Greys' | 'YlGnBu' | 'Greens' | 'YlOrRd' | 'Bluered' | 'RdBu' |
            #'Reds' | 'Blues' | 'Picnic' | 'Rainbow' | 'Portland' | 'Jet' |
            #'Hot' | 'Blackbody' | 'Earth' | 'Electric' | 'Viridis' |
            colorscale='YlGnBu',
            reversescale=True,
            color=[],
            size=10,
            colorbar=dict(
                thickness=15,
                title='Article Connections',
                xanchor='left',
                titleside='right'
            ),
            line=dict(width=2)))

    # Provide node details to plotly
    for node in G.nodes():
        x, y = G.node[node]['pos']
        node_trace['x'] += tuple([x])
        node_trace['y'] += tuple([y])
    for node, adjacencies in enumerate(G.adjacency()):
        node_trace['marker']['color'] += tuple([len(adjacencies[1])])
        node_info = 'Title: ' + str(titles[node]) + '.  No. of connections: ' \
                    + str(len(adjacencies[1]))
        node_trace['text'] += tuple([node_info])

    # Convert edges in networkx to plotly things
    edge_trace = go.Scatter(
        x=[],
        y=[],
        line=dict(width=1.5, color='#888'),
        hoverinfo='none',
        text=[],
        mode='lines')

    # This is for the hover function of the graph
    middle_node_trace = go.Scatter(
        x=[],
        y=[],
        text=[],
        mode='markers',
        hoverinfo='text',
        textposition='top center'
    )
    # This is for the edge labels
    edge_text_trace = go.Scatter(
        x=[],
        y=[],
        text=[],
        mode='text',
        hoverinfo=None,
        textposition='top center'
    )
    # Convert edges in networkx to plotly things
    for edge in G.edges():
        x0, y0 = G.node[edge[0]]['pos']
        x1, y1 = G.node[edge[1]]['pos']
        edge_trace['x'] += tuple([x0, x1, None])
        edge_trace['y'] += tuple([y0, y1, None])
        full_info = G.get_edge_data(edge[0], edge[1])['labels']
        middle_node_trace['text'] += tuple([full_info])
        middle_node_trace['x'] += tuple([(x0+x1)/2])
        middle_node_trace['y'] += tuple([(y0+y1)/2])
        if len(full_info) > 12:
            brief_info = full_info[0:12] + "..."
        else:
            brief_info = full_info
        edge_text_trace['text'] += tuple([brief_info])
        edge_text_trace['x'] += tuple([(x0+x1)/2])
        edge_text_trace['y'] += tuple([(y0+y1)/2])

    # Produce the graph in html
    fig = go.Figure(data=[edge_trace, node_trace, middle_node_trace, edge_text_trace],
                    layout=go.Layout(
                        title='<br>Common Keyword Graph',
                        titlefont=dict(size=16),
                        showlegend=False,
                        hovermode='closest',
                        margin=dict(b=20, l=5, r=5, t=40),
                        xaxis=dict(showgrid=False, zeroline=False, showticklabels=False),
                        yaxis=dict(showgrid=False, zeroline=False, showticklabels=False)))
    plot(fig, filename=output_file, auto_open=False)
