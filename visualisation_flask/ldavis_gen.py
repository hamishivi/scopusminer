'''
Module for generating the LDAVis visualisation.
'''
import re
import os

import pandas as pd
# Gensim
import gensim
import gensim.corpora as corpora # pylint: disable=C0414
from gensim.utils import simple_preprocess
# Plotting tools
import pyLDAvis
import pyLDAvis.gensim  # don't skip this
# spacy for lemmatization
import spacy

# do our setup and cleaning
def generate_lda_vis(filename, output_file, num_topics=20):
    '''
    Entry point for script.
    Num topics is the amount of groups LDA tries to split things into.
    '''
    if not os.path.exists(filename):
        print('file', filename, 'not found')
        return
    if num_topics <= 1:
        print("Invalid number of topics!")
        return
    df = pd.read_csv(filename)
    df = df.dropna()
    df['abstract'] = df['abstract'].astype(str)
    df['title'] = df['title'].astype(str)
    df['subjects'] = df['subjects'].apply(lambda x: re.sub(r'[^A-Za-z,\- ]', '', x).split(', '))
    df['abstract'] = df['abstract'].apply(lambda x: re.sub(r'[\xc2\xa9][^\.]*\.', ' ', x))

    data_words = df.abstract.values.tolist()

    # NLTK Stop words setup
    from nltk.corpus import stopwords
    stop_words = stopwords.words('english')
    stop_words.extend(['from', 'subject', 're', 'edu', 'use'])

    # Build the bigram and trigram models
    bigram = gensim.models.Phrases(data_words, min_count=5, threshold=100)

    # Faster way to get a sentence clubbed as a trigram/bigram
    bigram_mod = gensim.models.phrases.Phraser(bigram)

    # Define functions for stopwords, bigrams, trigrams and lemmatization
    def remove_stopwords(texts):
        return [[word for word in simple_preprocess(str(doc)) if word not in stop_words] \
                 for doc in texts]

    def make_bigrams(texts):
        return [bigram_mod[doc] for doc in texts]

    def lemmatization(texts, allowed_postags=['NOUN', 'ADJ', 'VERB', 'ADV']):
        """https://spacy.io/api/annotation"""
        texts_out = []
        for sent in texts:
            doc = nlp(" ".join(sent))
            texts_out.append([token.lemma_ for token in doc if token.pos_ in allowed_postags])
        return texts_out

    # Remove Stop Words
    data_words_nostops = remove_stopwords(data_words)

    # Form Bigrams
    data_words_bigrams = make_bigrams(data_words_nostops)

    # Initialize spacy 'en' model, keeping only tagger component (for efficiency)
    # python3 -m spacy download en
    nlp = spacy.load('en', disable=['parser', 'ner'])

    # Do lemmatization keeping only noun, adj, vb, adv
    data_lemmatized = lemmatization(data_words_bigrams,
                                    allowed_postags=['NOUN', 'ADJ', 'VERB', 'ADV'])

    # Create Dictionary
    id2word = corpora.Dictionary(data_lemmatized)

    # Create Corpus
    texts = data_lemmatized

    # Term Document Frequency
    corpus = [id2word.doc2bow(text) for text in texts]

    # Build LDA model
    lda_model = gensim.models.ldamodel.LdaModel(corpus=corpus,
                                                id2word=id2word,
                                                num_topics=num_topics,
                                                random_state=100,
                                                update_every=1,
                                                chunksize=100,
                                                passes=10,
                                                alpha='auto',
                                                per_word_topics=True)

    vis = pyLDAvis.gensim.prepare(lda_model, corpus, id2word)

    pyLDAvis.save_html(vis, output_file)
