'''
Integration tests for ldavis.
'''
import pytest

from flask import url_for
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

from visualisation_flask import create_app

@pytest.fixture
def app():
    app = create_app()
    app.config['TESTING'] = True
    yield app

@pytest.fixture
def driver():
    caps = DesiredCapabilities().CHROME
    caps["pageLoadStrategy"] = "normal"
    yield webdriver.Chrome(desired_capabilities=caps)

@pytest.mark.usefixtures('live_server')
def test_tree_retrieval(driver):
    '''
    just check we can load the page- since we are loading in LDAVis'
    generated html, we do not need to test it.
    '''
    driver.get(url_for('views.ldavis', _external=True))
    assert 'ldavis' in driver.title
