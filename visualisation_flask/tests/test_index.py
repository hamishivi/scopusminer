'''
Tests for index page.
'''

import pytest

from visualisation_flask import create_app

@pytest.fixture
def client():
    app = create_app()
    app.config['TESTING'] = True
    client = app.test_client()
    yield client

def test_index(client):
    response = client.get('/')
    assert response.status_code == 200
    assert response.content_type == 'text/html; charset=utf-8'
    assert b'Scopus Visualiser' in response.get_data()

def test_lda_load(client):
    response = client.get('/ldavis')
    assert response.status_code == 200
    assert response.content_type == 'text/html; charset=utf-8'
    assert b'LDAVis' in response.get_data()

def test_tree(client):
    response = client.get('/tree')
    assert response.status_code == 200
    assert response.content_type == 'text/html; charset=utf-8'
    assert b'data/fishbone_lda.json' in response.get_data() #reading correct file

def test_fishbone(client):
    response = client.get('/fishbone')
    assert response.status_code == 200
    assert response.content_type == 'text/html; charset=utf-8'
    assert b'Scopus' in response.get_data()

def test_bubble(client):
    response = client.get('/bubble')
    assert response.status_code == 200
    assert response.content_type == 'text/html; charset=utf-8'
    assert b'bubble' in response.get_data()



## README
# rather than testing the html direction, in get_data,
# I would recommend breaking up your own modules and testing them independently,
# and then testing the generation function.
# only test via the client if you need to test the website's running itself, such as
# how redirects work, or to test how you parse query strings.
# The idea is to test small chunks of code only - unit tests.
# then write a few bigger tests that fully run the program (that is, click buttons and
# navigate the website - 'integration tests').
