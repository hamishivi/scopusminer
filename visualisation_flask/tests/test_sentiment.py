'''
Tests for ldavis
'''
import os

import pytest
from visualisation_flask import create_app, sentiment_html_gen

@pytest.fixture
def client():
    app = create_app()
    app.config['TESTING'] = True
    client = app.test_client()
    yield client

def test_nonexist_file():
    '''
    Test nonexistent file handling
    '''
    try:
        sentiment_html_gen.generate_sentiment_data('fake.csv', 'test.html')
    except Exception:
        assert False

    assert os.path.exists('test.html')
    os.remove('test.html')

def test_empty_file():
    '''
    Test empty file handling
    '''
    try:
        sentiment_html_gen.generate_sentiment_data('../test_data/empty.csv', 'test.html')
    except Exception:
        assert False

    assert os.path.exists('test.html')
    os.remove('test.html')


def test_no_data():
    '''
    Test no data (but correct columns)
    '''
    try:
        sentiment_html_gen.generate_sentiment_data('../test_data/no-data.csv', 'test.html')
    except Exception:
        assert False

    assert os.path.exists('test.html')
    os.remove('test.html')

def test_generator_output_extreme_cutoff():
    '''
    Test extreme cutoff values
    '''
    try:
        sentiment_html_gen.generate_sentiment_data('../test_data/test_set.csv', 'test.html', cutoff=1.0)    
    except Exception:
        assert False
    
    assert os.path.exists('test.html')
    os.remove('test.html')

def test_generator_output():
    '''
    Test output is generated for sample data
    '''
    sentiment_html_gen.generate_sentiment_data('../test_data/test_set.csv', 'test.html',
                                    empath=False, lemmatized=False)
    assert os.path.exists('test.html')
    os.remove('test.html')


def test_generator_output_empath():
    '''
    Test output is generated for sample data
    '''
    sentiment_html_gen.generate_sentiment_data('../test_data/test_set.csv', 'test.html',
                                    empath=True, lemmatized=False)
    assert os.path.exists('test.html')
    os.remove('test.html')


def test_generator_output_lemmatized():
    '''
    Test output is generated for sample data
    '''
    sentiment_html_gen.generate_sentiment_data('../test_data/test_set.csv', 'test.html',
                                    empath=False, lemmatized=True)
    assert os.path.exists('test.html')
    os.remove('test.html')

def test_generator_output_cutoff():
    '''
    Test output is generated for sample data
    '''
    sentiment_html_gen.generate_sentiment_data('../test_data/test_set.csv', 'test.html',
                                    empath=False, lemmatized=False, cutoff=0.3)
    assert os.path.exists('test.html')
    os.remove('test.html')
