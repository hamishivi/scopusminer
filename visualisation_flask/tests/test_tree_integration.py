'''
Integration tests for the tree visualisation.
'''
import pytest

from flask import url_for
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

from visualisation_flask import create_app

@pytest.fixture
def app():
    app = create_app()
    app.config['TESTING'] = True
    yield app

@pytest.fixture
def driver():
    caps = DesiredCapabilities().CHROME
    caps["pageLoadStrategy"] = "normal"
    yield webdriver.Chrome(desired_capabilities=caps)

@pytest.mark.usefixtures('live_server')
def test_tree_retrieval(driver):
    '''
    get a subspine and ensure we have topics.
    to get lower topics require clicking, so we do not check these.
    We know the json is fine, and the tree library was external,
    so we don't need to test with clicking regardless.
    '''
    driver.get(url_for('views.tree', _external=True))
    assert 'Tree' in driver.title
    tree_el = driver.find_element_by_id('tree').text
    assert 'Computer Science all' in tree_el
