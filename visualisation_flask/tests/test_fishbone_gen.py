'''
Tests for fishbone generation.
'''
import json
import os
import pytest
from visualisation_flask import create_app, fishbone_data_gen

@pytest.fixture
def client():
    app = create_app()
    app.config['TESTING'] = True
    client = app.test_client()
    yield client

def test_basic_index(client):
    response = client.get('/')
    assert response.status_code == 200
    assert response.content_type == 'text/html; charset=utf-8'
    assert b'Scopus' in response.get_data()

def test_empty_file():
    '''
    Test empty file handling.
    '''
    try:
        fishbone_data_gen.generate_fishbone_data('fake.csv', 'fake.out')
    except Exception:
        assert False
    assert True

def test_generator_output():
    '''
    This test merely is checking for the correct fishbone structure,
    rather than algorithm correctness. Since we are using an external
    library for the algorithm, the structure is the main element we care about.
    '''
    fishbone_data_gen.generate_fishbone_data('../test_data/test_set.csv', 'test.json')
    with open('test.json') as f:
        j = json.loads(f.read())
        # check basic name
        assert j['name'] == "Scopus"
        # check children length
        assert len(j['children']) == 22
        # check subchildren exist
        for child in j['children']:
            assert len(child['name']) > 0
            assert len(child['children']) > 0
            # check each subchild has a name and children
            for subchild in child['children']:
                assert len(subchild['name']) > 0
                assert len(subchild['children']) > 0
    os.remove('test.json')

def test_generator_output_one_topic():
    '''
    Test to ensure that the number of topics parameter is used.
    '''
    fishbone_data_gen.generate_fishbone_data('../test_data/test_set.csv', 'test.json', num_topics=1)
    with open('test.json') as f:
        j = json.loads(f.read())
        # check subchildren exist
        for child in j['children']:
            # check only one subtopic
            assert len(child['children']) == 1
    os.remove('test.json')

def test_fishbone_load_data(client):
    '''
    This test lightly checks we can retrieve pages correctly.
    '''
    response = client.get('/fishbone')
    # check this is the main spine
    assert b'Scopus' in response.get_data()