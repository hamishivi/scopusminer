'''
Tests for keyword graph
'''
import os

import pytest
from visualisation_flask import create_app, keyword_graph_gen

@pytest.fixture
def client():
    app = create_app()
    app.config['TESTING'] = True
    client = app.test_client()
    yield client


def test_empty_file():
    """
    Test empty file handling
    """
    try:
        keyword_graph_gen.generate_keyword_graph('fake.csv', 'fake.out')
    except Exception:
        assert False
    assert True


def test_output_html():
    """
    Test whether a html is output
    """
    keyword_graph_gen.generate_keyword_graph('../test_data/test_set.csv', 'test.html')
    # Ensure a html file has been produced
    assert os.path.exists('test.html')
    with open('test.html') as f:
        file = f.read()
        # Ensure the description of the graphs are in the files
        assert 'Common Keyword Graph' in file
        assert 'Article Connection' in file
        # Ensure the first and last title are displayed
        assert 'GHUMVEE: Efficient, effective, and flexible replication' in file
        assert 'Bluff-probe based black hole node detection and prevention' in file
    os.remove('test.html')


def test_output_html_one_article():
    """
    Test whether the function can limit the number of articles displayed in the graph
     """
    keyword_graph_gen.generate_keyword_graph('../test_data/test_set.csv', 'test.html', top_k=2)
    # Ensure a html file has been produced
    assert os.path.exists('test.html')
    with open('test.html') as f:
        file = f.read()
        # Ensure only the first two articles are displayed
        assert 'GHUMVEE: Efficient, effective, and flexible replication' in file
        assert 'Using received signal strength indicator to ' \
               'detect node replacement and replication attacks in wireless sensor networks' in file
        assert 'DDoS flood attack detection based on fractal parameters' not in file
    os.remove('test.html')
