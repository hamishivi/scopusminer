'''
Tests for ldavis
'''
import os

import pytest
from visualisation_flask import create_app, ldavis_gen

@pytest.fixture
def client():
    app = create_app()
    app.config['TESTING'] = True
    client = app.test_client()
    yield client

def test_empty_file():
    '''
    Test empty file handling
    '''
    try:
        ldavis_gen.generate_lda_vis('fake.csv', 'fake.out')
    except Exception:
        assert False
    assert True

# simply test it ran fully
def test_generator_output():
    ldavis_gen.generate_lda_vis('../test_data/test_set.csv', 'test.html')
    assert os.path.exists('test.html')
    with open('test.html') as f:
        file = f.read()
        assert 'Topic1' in file
        assert 'Topic20' in file
    os.remove('test.html')

def test_generator_output_one_topic():
    '''
    Test to ensure that the number of topics parameter is used.
    '''
    ldavis_gen.generate_lda_vis('../test_data/test_set.csv', 'test.html', num_topics=2)
    with open('test.html') as f:
        file = f.read()
        assert 'Topic1' in file
        assert 'Topic3' not in file
    os.remove('test.html')

def test_fishbone_load_data(client):
    '''
    This test lightly checks we can retrieve pages correctly.
    '''
    response = client.get('/ldavis')
    # check this is the main spine
    assert b'ldavis' in response.get_data()
