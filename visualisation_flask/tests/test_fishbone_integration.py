'''
Integration tests for fishbone visualisation.
'''
import re
import json

import pytest
from flask import url_for
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

from visualisation_flask import create_app

@pytest.fixture
def app():
    app = create_app()
    app.config['TESTING'] = True
    yield app

@pytest.fixture
def driver():
    caps = DesiredCapabilities().CHROME
    caps["pageLoadStrategy"] = "normal"
    yield webdriver.Chrome(desired_capabilities=caps)

@pytest.mark.usefixtures('live_server')
def test_fishbone_topics(driver):
    '''
    Test to ensure we can get main fishbone
    '''
    # get a subspine and ensure we have topics
    driver.get(url_for('views.fishbone', _external=True))
    assert 'main' in driver.title
    # there should be no subtopics in the below list
    print(driver.find_element_by_id('fishbone').text)
    matches = re.findall('[A-Za-z]+,[A-Za-z]+,[A-Za-z]+,[A-Za-z]+,[A-Za-z]+', driver.find_element_by_id('fishbone').text)
    assert len(matches) == 0

@pytest.mark.usefixtures('live_server')
def test_fishbone_subtopics(driver):
    '''
    Test to ensure we can get a sub-fishbone
    '''
    # get a subspine and ensure we have topics
    driver.get(url_for('views.fishbone', topic='Computer Science all', _external=True))
    assert 'Computer Science all' in driver.title
    # this should only match to the subtopic names, which are five words combined by commas
    matches = re.findall('[A-Za-z]+,[A-Za-z]+,[A-Za-z]+,[A-Za-z]+,[A-Za-z]+', driver.find_element_by_id('fishbone').text)
    assert len(matches) == 5

# The below test times out on CircleCI, so it is commented out here. This test does pass given enough time.
# CircleCI simply does not provide a fast enough performance for the test to run without timing out.
'''
@pytest.mark.usefixtures('live_server')
def test_fishbone_subtopics_one_topic(driver):
    """
    Test to ensure we can get sub-fishbone with different amount of topics
    """
    # first, save the old json to restore it later
    with open('static/data/fishbone_lda.json') as f:
        data = json.load(f)
    # get a subspine and ensure we have one topic
    driver.implicitly_wait(50)
    driver.get(url_for('views.fishbone', num_topics='1', topic='Computer Science all', _external=True))
    assert 'Computer Science all' in driver.title
    # this should only match to the subtopic names, which are five words combined by commas
    matches = re.findall('[A-Za-z]+,[A-Za-z]+,[A-Za-z]+,[A-Za-z]+,[A-Za-z]+', driver.find_element_by_id('fishbone').text)
    assert len(matches) == 1
    # reset json back to old form
    with open('static/data/fishbone_lda.json', 'w') as f:
        json.dump(data, f)
'''
@pytest.mark.usefixtures('live_server')
def test_fishbone_subsubtopics(driver):
    '''
    Test to ensure we can get a sub-sub-spine
    '''
    # get a subsubspine and ensure we have have a list of papers
    driver.get(url_for('views.fishbone', topic='sybil,vast,segregate,moment,cautionary', _external=True))
    assert 'sybil,vast,segregate,moment,cautionary' in driver.title
    assert 'Quantifying resistance to the sybil attack' in  driver.find_element_by_class_name('collection').text
