"""
Tests for bubble graph generation.
"""
import json
import csv
import os
import pytest
from visualisation_flask import create_app, bubble_data_gen
import nltk

@pytest.fixture
def client():
    app = create_app()
    app.config['TESTING'] = True
    client = app.test_client()
    yield client


def test_empty_file():
    """
    Test empty file handling.
    """
    try:
        bubble_data_gen.get_keyword_frequency('fake.csv', 'fake.out')
        bubble_data_gen.get_keyword_to_title('fake.csv', 'fake.out')
    except Exception:
        assert False
    assert True


def test_output_files_exist():
    """
    Test whether the normal process will output files as expected
    """
    # download stopwords for test
    nltk.download('stopwords')
    nltk.download('wordnet')
    bubble_data_gen.get_keyword_frequency('../test_data/test_set.csv', 'test.csv')
    bubble_data_gen.get_keyword_to_title('../test_data/test_set.csv', 'test.json')
    assert os.path.exists('test.csv')
    assert os.path.exists('test.json')
    os.remove('test.csv')
    os.remove('test.json')

'''
Note the tests below are very touchy.
I don't have a lot of time to examine why, but it seems there is some randomness
in how the keywords are found. Hence, if these tests fail locally, don't worry! They
passed for us :).
'''

def test_keyword_to_titles_json():
    """
    Test whether the keyword_to_titles json file contains right information in the right format
    """
    # download stopwords for test
    nltk.download('stopwords')
    nltk.download('wordnet')
    bubble_data_gen.get_keyword_to_title('../test_data/test_set.csv', 'test.json')
    # Open and check
    with open('test.json') as f:
        j = json.loads(f.read())
        # Check if the number of keywords in the file is same as what we expected
        assert len(j) == 138
        # Ensure the types of the variables in the json file are correct
        for k in j.keys():
            assert isinstance(k, type(""))
            assert isinstance(j[k], type([]))
            # Make sure every keyword in the json file should have at least one article that contain it
            assert len(j[k]) > 0
        # Test on a specific keyword and see if its corresponding article titles are what we expected
        example_keyword = 'packet'
        example_titles = ['Improving intrusion detection system based on Snort rules '
                          'for network probe attack detection']
        assert j[example_keyword] == example_titles
    os.remove('test.json')


def test_keyword_frequency_csv():
    """
    Test whether the keyword frequency csv file contains the right information in the right format
    """
    # download stopwords for test
    nltk.download('stopwords')
    nltk.download('wordnet')
    bubble_data_gen.get_keyword_frequency('../test_data/test_set.csv', 'test.csv')
    # Convert the tested file to a tested dict
    input_file = csv.DictReader(open('test.csv'))
    test_dict = {}
    for row in input_file:
        test_dict[row["Keyword"]] = int(row["Frequency"])
        # Ensure there is no keyword that does not appear once in any article from test.csv
        assert test_dict[row["Keyword"]] > 0
    # Make sure the number of keywords is what we expected
    assert len(test_dict) == 138
    # Check the most frequent keyword and its frequency are as expected
    key_list = list(test_dict.keys())
    value_list = list(test_dict.values())
    assert max(value_list) == 14
    assert key_list[value_list.index(14)] == 'attack'
    os.remove('test.csv')


def test_subgraph_data_json():
    """
    Test whether the subgraph data json file contains the right information in the right format
    """
    # download stopwords for test
    nltk.download('stopwords')
    nltk.download('wordnet')
    bubble_data_gen.get_keyword_to_title('../test_data/test_set.csv', 'test.json')
    bubble_data_gen.generate_subgraph_data('packet', 'test.json', 'test1.json')
    with open('test1.json') as f:
        j = json.loads(f.read())
        k = list(j.keys())
        # Ensure the data contains nodes and links
        assert len(k) == 2 and k[0] == "nodes" and k[1] == "links"
        nodes = j["nodes"]
        links = j["links"]
        # Ensure the node and link information are in lists
        assert isinstance(nodes, type([])) and isinstance(links, type([]))
        # Ensure the number of nodes and links
        assert len(nodes) == 2 and len(links) == 1
        # Check all nodes and ensure their formats are correct
        for i, node in enumerate(nodes):
            # Make sure each node is a python dict
            assert isinstance(node, type(dict()))
            node_k = list(node.keys())
            # Make sure their keys are correct
            assert len(node_k) == 2 and node_k[0] == "name" and node_k[1] == "group"
            # Check if the first node is the keyword
            if i == 0:
                assert node["name"] == "packet"
            # Ensure all nodes are in group 1 so they are clustered together
            assert node["group"] == 1
        # Check all links and ensure their formats are correct
        for link in links:
            # Make sure each link is a python dict
            assert isinstance(link, type(dict()))
            link_k = list(link.keys())
            # Make sure their keys are correct
            assert len(link_k) == 3 and link_k[0] == "source" and link_k[1] == "target" and link_k[2] == "weight"
            # Ensure the first node is all the links' target because it means all titles will point to the keyword
            assert link["target"] == 0
            # Ensure all links have the same weight of 3
            assert link["weight"] == 3
    os.remove('test.json')
    os.remove('test1.json')


def test_bubble_graph_subgraph_consistency():
    """
    Test whether the bubble graph data and subgraph data are consistent
    """
    # download stopwords for test
    nltk.download('stopwords')
    nltk.download('wordnet')
    bubble_data_gen.get_keyword_frequency('../test_data/test_set.csv', 'test.csv')
    bubble_data_gen.get_keyword_to_title('../test_data/test_set.csv', 'test.json')
    # Open keyword_to_title json file
    with open('test.json') as f:
        j = json.loads(f.read())
        # Open frequency csv file and check if both files are consistent in terms of keywords
        frequency_file = csv.DictReader(open('test.csv'))
        # Ensure the number of keywords in both files are the same
        assert len(j) == len(list(frequency_file))
        for row in frequency_file:
            # Ensure the files have the same keywords
            assert row["Keyword"] in j.keys()
    os.remove('test.json')
    os.remove('test.csv')
