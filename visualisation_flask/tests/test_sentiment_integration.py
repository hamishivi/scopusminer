'''
Integration tests for sentiment.
'''
import pytest

from flask import url_for
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

from visualisation_flask import create_app

@pytest.fixture
def app():
    app = create_app()
    app.config['TESTING'] = True
    yield app

@pytest.fixture
def driver():
    caps = DesiredCapabilities().CHROME
    caps["pageLoadStrategy"] = "normal"
    yield webdriver.Chrome(desired_capabilities=caps)

@pytest.mark.usefixtures('live_server')
def test_sentiment_retrieval(driver):
    '''
    Check that pre-made diagrams can be loaded - since we are loading in
    generated html, we only need to test that custom output is not loaded.
    '''
    driver.get(url_for('views.sentiment', lemmatized=False, empath=False, cutoff=0.2, _external=True))
    assert 'Custom' not in driver.title

    driver.get(url_for('views.sentiment', lemmatized=True, empath=False, cutoff=0.2, _external=True))
    assert 'Custom' not in driver.title

    driver.get(url_for('views.sentiment', lemmatized=False, empath=True, cutoff=0.2, _external=True))
    assert 'Custom' not in driver.title

# The below times out on CircleCI, but would test that custom diagram creation is successful
'''
@pytest.mark.usefixtures('live_server')
def test_custom_retrieval(driver):
    """
    Since we are loading in generated html, we need to test that custom output is loaded
    """
    driver.get(url_for('views.sentiment', lemmatized=True, empath=True, cutoff=0.6, _external=True))
    assert 'Custom' in driver.title
'''