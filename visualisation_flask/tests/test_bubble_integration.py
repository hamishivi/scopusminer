'''
Integration tests for ldavis.
'''
import pytest

from flask import url_for
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

from visualisation_flask import create_app


@pytest.fixture
def app():
    app = create_app()
    app.config['TESTING'] = True
    yield app


@pytest.fixture
def driver():
    caps = DesiredCapabilities().CHROME
    caps["pageLoadStrategy"] = "normal"
    yield webdriver.Chrome(desired_capabilities=caps)


@pytest.mark.usefixtures('live_server')
def test_bubble_graph_load(driver):
    """
    Check if we can load the bubble graph page
    """
    driver.get(url_for('views.bubble', _external=True))
    assert 'Bubble' in driver.title


@pytest.mark.usefixtures('live_server')
def test_bubble_subgraph_load(driver):
    """
    Check if we can load the subgraph page
    """
    driver.get(url_for('views.bubble', keyword='packet', _external=True))
    # Ensure it goes to a non-main page
    assert '' in driver.title
