var padding = 1.5, // separation between same-color nodes
    clusterPadding = 6, // separation between different-color nodes
    maxRadius = 12;

function getSize(d) {
    var bbox = this.getBBox(),
        cbbox = this.parentNode.getBBox(),
        scale = Math.min(cbbox.width/bbox.width, cbbox.height/bbox.height);
    d.scale = scale;
}

function ticked() {
    bubbles.each(function (node) {})
    .attr("cx", function(d) { return d.x; })
    .attr("cy", function(d) { return d.y; });
}

var forceSim = null

function addForceLayout(isStatic, nodes) {
    if (forceSim) {
        // Stop any forces currently in progress
        forceSim.stop();
    }
    // Configure the force layout holding the bubbles apart
    forceSim = d3.forceSimulation()
                .nodes(nodes)
                .velocityDecay(0.3)
                .on("tick", ticked);

    if (!isStatic) {
        // Decide what kind of force layout to use: "collide" or "charge"
        if (force_type == "collide") {
            var bubbleCollideForce = d3.forceCollide()
                .radius(function (d) {
                    return d.r + 0.5;
                }).iterations(4)
            forceSim.force("collide", bubbleCollideForce)
        }
        if (force_type == "charge") {
            function bubbleCharge(d) {
                return -Math.pow(d.r, 2.0) * 0.03;
            }
            forceSim.force('charge', d3.forceManyBody().strength(bubbleCharge));
        }
    }
}

function bubbleChart(data, width, height) {
    d3.select("#bubble").select("svg").remove()
    var color = d3.scale.category20b() ; //color category
    var forceSim = null;

    var bubble = d3.layout.pack()
        .sort(null)
        .size([width, height])
        .padding(1.5);


    var tooltip = d3.select("#bubble")
        .append("div")
        .style("position", "absolute")
        .style("visibility", "hidden")
        .style("color", "white")
        .style("padding", "8px")
        .style("background-color", "#626D71")
        .style("border-radius", "6px")
        .style("text-align", "center")
        .style("font-family", "monospace")
        .style("width", "400px")
        .text("");

    var svg = d3.select("#bubble")
        .append("svg")
        .attr("width", width)
        .attr("height", height)
        .attr("class", "bubble");

    //convert numerical values from strings to numbers
    data = data.map(function(d){
        d.id = d["Keyword"];
        d.value = +d["Frequency"];
        return d;
    });

    //bubbles needs very specific format, convert data to this.
    var nodes = bubble.nodes({children:data}).filter(function(d) { return !d.children; });
    //setup the chart
    var bubbles = svg.append("g")
        .selectAll(".bubble")
        .data(nodes)
        .enter();

    //create the bubbles
    bubbles.append("circle")
        .attr("r", function(d){ return d.r; })
        .attr("cx", function(d){ return d.x; })
        .attr("cy", function(d){ return d.y; })
        .style("fill", function(d) { return color(d.value); })
        .on("mouseover", function(d) {
            d3.select(this).attr('stroke', 'black').attr('stroke-width', 5);
            tooltip.html("Keyword: " + d.id + "<br>Frequency: " + d.value + " times" + "<br>");
            return tooltip.style("visibility", "visible");
        })
        .on("mousemove", function() {
            return tooltip.style("top", (d3.event.pageY - 10) + "px").style("left", (d3.event.pageX + 10) + "px");
        })
        .on("mouseout", function() {
            d3.select(this).attr('stroke', 0).attr('stroke-width', 0);
            return tooltip.style("visibility", "hidden");
        })
        .on("click", function(d) {
          window.location.href = '/bubble?keyword=' + d.id
        });

    bubbles.append("text")
        .attr("x", function(d) { return d.x; })
        .attr("y", function(d) { return d.y; })
        .attr("text-anchor", "middle")
        .text(function(d){  return d.id.substring(0, d.r / 4.5); })

        .on("mouseover", function(d) {
            cls = d3.selectAll("circle")[0]
            corresponding_circle = cls.find(function(element) { return element.__data__.id == d.id})
            d3.select(corresponding_circle).attr('stroke', 'black').attr('stroke-width', 5);
            tooltip.html("Keyword: " + d.id + "<br>Frequency: " + d.value + " times");
            return tooltip.style("visibility", "visible");
        })
        .on("mousemove", function() {
            return tooltip.style("top", (d3.event.pageY - 10) + "px").style("left", (d3.event.pageX + 10) + "px");
        })
        .on("mouseout", function() {
            d3.select(corresponding_circle).attr('stroke', 0).attr('stroke-width', 0);
            return tooltip.style("visibility", "hidden");
        })
        .on("click", function(d) {
          window.location.href = '/bubble?keyword=' + d.id
        })
        .style("font-size", function(d) { return  d.r/28 * 15 + "px"; })
        .attr("dy", ".20em");
    //d3.forceSimulation().nodes(d3.selectAll("circle")[0])


    /*node = d3.selectAll("circle")
    var force = d3.layout.force()
        .nodes(nodes)
        .size([width, height])
        .gravity(.02)
        .charge(0)
        .on("tick", tick)
        .start();

    function tick(e) {
        node.each(collide(.5))
            .attr("transform", function (d) {
                var k = "translate(" + d.x + "," + d.y + ")";
                return k;
            })
    }

    // Resolves collisions between d and all other circles.
    function collide(alpha) {
        var quadtree = d3.geom.quadtree(nodes);
        return function (d) {
            var r = d.radius + maxRadius + Math.max(padding, clusterPadding),
                nx1 = d.x - r,
                nx2 = d.x + r,
                ny1 = d.y - r,
                ny2 = d.y + r;
            quadtree.visit(function (quad, x1, y1, x2, y2) {
                if (quad.point && (quad.point !== d)) {
                    var x = d.x - quad.point.x,
                        y = d.y - quad.point.y,
                        l = Math.sqrt(x * x + y * y),
                        r = d.radius + quad.point.radius + (d.cluster === quad.point.cluster ? padding : clusterPadding);
                    if (l < r) {
                        l = (l - r) / l * alpha;
                        d.x -= x *= l;
                        d.y -= y *= l;
                        quad.point.x += x;
                        quad.point.y += y;
                    }
                }
                return x1 > nx2 || x2 < nx1 || y1 > ny2 || y2 < ny1;
            });
        };
    }*/


}