'''
Module for generating the bubble chart.
'''
import os
import json
import pandas as pd
from summa import keywords as k
from nltk.stem import WordNetLemmatizer
from nltk.corpus import stopwords


def preprocess(kws):
    """
    Lemmatize the input keyword list
    """
    wordnet_lemmatizer = WordNetLemmatizer()
    kws = [wordnet_lemmatizer.lemmatize(k.lower()) for k in kws]
    kws = list(set(kws))
    return kws


def get_keyword_frequency(filename, output_file):
    '''
    Main entry point for this module. Generates a html
    bubble chart in the output file from the given file.
    '''
    if not os.path.exists(filename):
        print('could not find', filename)
        return
    dataframe = pd.read_csv(filename)
    # Lemmatizer for grouping similar words, e.g. removing plural form
    wordnet_lemmatizer = WordNetLemmatizer()
    stop_words = set(stopwords.words('english'))
    stop_words.add('like')
    stop_words.add('unlike')
    keyword_dict = dict()
    for abstract in dataframe.abstract:
        try:
            keywords = k.keywords(abstract)
        except ValueError:
            continue
        keywords = keywords.splitlines()
        # Accumulate the keywords' occurrence
        for keyword in keywords:
            # Lemmatization for each keyword
            keyword = wordnet_lemmatizer.lemmatize(keyword.lower())
            if keyword in stop_words or len(keyword) < 2:
                continue
            if keyword in keyword_dict:
                keyword_dict[keyword] += 1
            else:
                keyword_dict[keyword] = 1
    data = {'Keyword': list(keyword_dict.keys()), 'Frequency': list(keyword_dict.values())}
    df = pd.DataFrame(data)
    df.to_csv(output_file, index=False)


def get_keyword_to_title(filename, output_file):
    """
    Get the data for the bubble chart sub graph
    """
    if not os.path.exists(filename):
        print('could not find', filename)
        return
    dataframe = pd.read_csv(filename)
    dataframe = dataframe.dropna()
    # Get titles and abstracts for later use
    article_titles = dataframe.title
    article_abstracts = dataframe.abstract
    # Add extra stop words here
    stop_words = set(stopwords.words('english'))
    stop_words.add('like')
    stop_words.add('unlike')
    # Dict to store the required data
    shared_keywords = {}
    for article_title, abstract in zip(article_titles, article_abstracts):
        try:
            keywords = k.keywords(abstract)
        except ValueError:
            continue
        keywords = keywords.splitlines()
        keywords = preprocess(keywords)
        for keyword in keywords:
            if len(keyword) < 2 or keyword in stop_words:
                continue
            if keyword in shared_keywords:
                shared_keywords[keyword].append(article_title)
            else:
                shared_keywords[keyword] = []
                shared_keywords[keyword].append(article_title)
    # Output a json file
    with open(output_file, 'w') as fp:
        json.dump(shared_keywords, fp)


def generate_subgraph_data(target_key, filename, output_file):
    """
    Instantly generate the subgraph information (a list of titles that contain the target keyword
    """
    if not os.path.exists(filename):
        print('could not find', filename)
        return
    # Read the full keyword to title list
    with open(filename, 'r') as f:
        in_dict = json.load(f)
    # Sub graph structure
    out_dict = {"nodes": [], "links": []}
    node_dict = {"name": target_key, "group": 1}
    out_dict["nodes"].append(node_dict)
    # Find our target keyword and grab the corresponding titles
    if target_key in in_dict.keys():
        for i, t in enumerate(in_dict[target_key]):
            node_dict = {"name": t, "group": 1}
            link_dict = {"source": (i + 1), "target": 0, "weight": 3}
            out_dict["nodes"].append(node_dict)
            out_dict["links"].append(link_dict)
    # Output the json file
    with open(output_file, 'w') as fp:
        json.dump(out_dict, fp)
