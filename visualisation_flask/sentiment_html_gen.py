'''
The module for generating the sentiment.
'''
import re
from shutil import copyfile
import os

import numpy as np
import pandas as pd
# spacy for lemmatization
import spacy
# gensim
import gensim
import scattertext as st
import nltk

def generate_sentiment_data(filename, output_file, cutoff=0.2, lemmatized=False, empath=False):
    '''
    Generates a html sentiment graph in output_file, based on the data in filename.
    Cutoff is used for determining whether a sentiment is positive or negative.
    lemmatized turns on or off lemmatization.
    empath turns on or off the use of empath topics.
    '''

    if not os.path.exists(filename):
        copyfile('./templates/emptyfile_error.html', output_file)
        print('Could not find', filename)
        return

    nltk.download('wordnet')
    nltk.download('stopwords')

    try:
        df = pd.read_csv(filename)
    except pd.errors.EmptyDataError:
        copyfile('./templates/emptyfile_error.html', output_file)
        print('No columns')
        return

    df = df.dropna()
    df = df.reset_index(drop=True)

    # If key columns don't exist, return error
    if not all(elem in list(df.columns.values) for elem in ['abstract', 'title']):
        copyfile('./templates/emptyfile_error.html', output_file)
        print('Columns missing')
        return

    # If file is empty, return early with a blank document
    if len(df) == 0: # pylint: disable=C1801
        copyfile('./templates/emptyfile_error.html', output_file)
        print('File missing/insufficient papers')
        return

    df['abstract'] = df['abstract'].astype(str)
    df['title'] = df['title'].astype(str)
    data = df.abstract.values.tolist()
    data = [re.sub(r'[\xc2\xa9][^\.]*\.', ' ', abstract) for abstract in data]
    df = sent_extraction(data, df)

    # If file only contains positive/negative papers, return error page
    num_positive = len(df[df['sen_compound'] > 0])
    num_negative = len(df[df['sen_compound'] <= 0])
    if(num_positive == 0 or num_negative == 0):
        copyfile('./templates/emptyfile_error.html', output_file)
        return

    # Before processing, check the cut-off is reasonable; otherwise set to 0
    num_positive = len(df[df['sen_compound'] > cutoff])
    num_negative = len(df[df['sen_compound'] <= (-1 * cutoff)])
    if(num_positive == 0 or num_negative == 0):
        cutoff = 0

    # Create categorical sentiment variable for papers.
    # For diagram only two can be chosen, but it may be important to consider
    # neutral as well. -0.6/0.6 splits the datapoints roughly 33/33/33 between
    # the categories for this set, based on the CDF
    conditions = [
        (df['sen_compound'] > cutoff),
        (df['sen_compound'] <= (-1 * cutoff))]
    choices = ["positive", "negative"]
    df['sentiment_category'] = np.select(conditions, choices, default='neutral')
    df = df.drop(df[df['sentiment_category'] == 'neutral'].index)

    # Reset index and remove empty rows (avoids issues with scattertext)
    df.reset_index(drop=True, inplace=True)

    # Set column for text to be graphed
    text_column = 'abstract'
    # If required, lemmatize and remove stopwords
    if lemmatized:
        df = lemmatize(df)
        text_column = 'lemmatized'

    # Generate HTML Scattertext diagram
    import en_core_web_sm
    nlp = en_core_web_sm.load()

    # nlp = spacy.load('en-core-web-sm')

    if not empath:
        corpus = st.CorpusFromPandas(df,
                                     category_col='sentiment_category',
                                     text_col=text_column,
                                     nlp=nlp).build()
        html = st.produce_scattertext_explorer(corpus,
                                               category='positive',
                                               category_name='Positive Sentiment',
                                               not_category_name='Negative Sentiment',
                                               width_in_pixels=1000,
                                               metadata=df['title'])

    else:
        feat_builder = st.FeatsFromOnlyEmpath()
        empath_corpus = st.CorpusFromParsedDocuments(df,
                                                     category_col='sentiment_category',
                                                     feats_from_spacy_doc=feat_builder,
                                                     parsed_col=text_column).build()
        html = st.produce_scattertext_explorer(empath_corpus,
                                               category='positive',
                                               category_name='Positive Sentiment',
                                               not_category_name='Negative Sentiment',
                                               width_in_pixels=1000,
                                               metadata=df['title'],
                                               use_non_text_features=True,
                                               use_full_doc=True,
                                               topic_model_term_lists=feat_builder\
                                                                      .get_top_model_term_lists())

    open(output_file, 'wb').write(html.encode('utf-8'))

def sent_extraction(data, df, verbose=False):
    '''takes a list of fields and a dataframe, and returns an
    updated dataframe with the sentiment analysis results appended.
    Assumes that the strings in 'data' line up with those in df.'''
    # Currently Using NLTK Vader Sentiment Analyzer:
    #  Hutto, C.J. & Gilbert, E.E. (2014). VADER: A Parsimonious Rule-based Model for
    #  Sentiment Analysis of Social Media Text. Eighth International Conference on
    #  Weblogs and Social Media (ICWSM-14). Ann Arbor, MI, June 2014.

    nltk.download('vader_lexicon')
    from nltk.sentiment.vader import SentimentIntensityAnalyzer
    sid = SentimentIntensityAnalyzer()
    sentiments = []

    # Extract sentiment for each abstract
    for abstract in data:
        ss = sid.polarity_scores(abstract)
        sentiments.append(ss)
        if verbose:
            for k in sorted(ss):
                print('\t {0}: {1}, '.format(k, ss[k]), end='')
            print('\n')

    # Append back to dataframe
    return df.assign(sen_compound=[x['compound'] for x in sentiments],
                     sen_neg=[x['neg'] for x in sentiments],
                     sen_neu=[x['neu'] for x in sentiments],
                     sen_pos=[x['pos'] for x in sentiments])

def lemmatize(df):
    '''
    Performs lemmatization.
    '''

    # NLTK Stop words
    nltk.download('stopwords')
    from nltk.corpus import stopwords

    # Stopword setup
    stop_words = stopwords.words('english')
    stop_words.extend(['from', 'subject', 're', 'edu', 'use'])

    data = df.abstract.values.tolist()
    data = [re.sub(r'[\xc2\xa9][^\.]*\.', ' ', abstract) for abstract in data]

    def sent_to_words(sentences):
        for sentence in sentences:
            yield gensim.utils.simple_preprocess(str(sentence), deacc=True)
    data_words = list(sent_to_words(data))

    # Build the bigram model
    bigram = gensim.models.Phrases(data_words, min_count=5, threshold=100)

    # Faster way to get a sentence clubbed as a trigram/bigram
    bigram_mod = gensim.models.phrases.Phraser(bigram)

    from gensim.utils import simple_preprocess

    def lemmatization(texts, allowed_postags=['NOUN', 'ADJ', 'VERB', 'ADV']):
        """https://spacy.io/api/annotation"""
        texts_out = []
        for sent in texts:
            doc = nlp(" ".join(sent))
            texts_out.append([token.lemma_ for token in doc if token.pos_ in allowed_postags])
        return texts_out

    def remove_stopwords(texts):
        return [[word for word in simple_preprocess(str(doc)) if word not in stop_words] \
                 for doc in texts]

    def make_bigrams(texts):
        return [bigram_mod[doc] for doc in texts]

    # Remove Stop Words
    data_words_nostops = remove_stopwords(data_words)

    # Form Bigrams
    data_words_bigrams = make_bigrams(data_words_nostops)

    # Initialize spacy 'en' model, keeping only tagger component (for efficiency)
    # python3 -m spacy download en
    nlp = spacy.load('en', disable=['parser', 'ner'])

    # Do lemmatization keeping only noun, adj, vb, adv
    data_lemmatized = lemmatization(data_words_bigrams,
                                    allowed_postags=['NOUN', 'ADJ', 'VERB', 'ADV'])
    lemmatized = [' '.join(x) for x in data_lemmatized]
    lemmatized_df = pd.DataFrame(lemmatized)

    df['lemmatized'] = lemmatized_df

    return df
