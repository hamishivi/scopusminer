'''
Router for flask app. This handles all routing.
'''
import os
import json

from werkzeug.utils import secure_filename
from flask import Blueprint, g, redirect, render_template, request, session, \
    current_app, send_file
from visualisation_flask import fishbone_data_gen, sentiment_html_gen, \
    keyword_graph_gen, ldavis_gen, bubble_data_gen

# setup app as blueprint
app = Blueprint('views', __name__,)

ALLOWED_EXTENSIONS = set(['csv'])

# please set your default files here
default_filename = './visualisation_flask/upload_folder/full_set_01.csv'
default_fishbone = True
default_sentiment = True
default_sentiment_empath = True
default_sentiment_lemmatized = True
default_keywords = True
default_bubble = True
default_topics = 20
default_articles = 10
default_cutoff = 0.2
default_ldavis = True


def allowed_file(filename):
    '''
    Ensure the file is allowed to be uploaded.
    This helps stop users try and abuse the file uploader.
    '''
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.before_request
def set_variables():
    '''
    Set our g variables based on if they are
    in the session or not.
    '''

    if 'filename' in session:
        g.filename = session['filename']
    else:
        g.filename = default_filename
    if 'fishbone' in session:
        g.fishbone = session['fishbone']
        g.num_topics = session['topics']
    else:
        g.fishbone = default_fishbone
        g.num_topics = default_topics
    if 'ldavis' in session:
        g.ldavis = session['ldavis']
        g.lda_num_topics = session['lda_topics']
    else:
        g.ldavis = default_ldavis
        g.lda_num_topics = default_topics
    if 'sentiment' in session:
        g.sentiment = session['sentiment']
        g.cutoff = session['cutoff']
    else:
        g.sentiment = default_sentiment
        g.cutoff = default_cutoff
    if 'sentiment_empath' in session:
        g.sentiment_empath = session['sentiment_empath']
        g.cutoff = session['cutoff']
    else:
        g.sentiment_empath = default_sentiment_empath
        g.cutoff = default_cutoff
    if 'sentiment_lemmatized' in session:
        g.sentiment_lemmatized = session['sentiment_lemmatized']
        g.cutoff = session['cutoff']
    else:
        g.sentiment_lemmatized = default_sentiment_lemmatized
        g.cutoff = default_cutoff
    if 'keywords' in session:
        g.keywords = session['keywords']
        g.num_articles = session['articles']
    else:
        g.keywords = default_keywords
        g.num_articles = default_articles
    if 'bubble' in session:
        g.bubble = session['bubble']
    else:
        g.bubble = default_bubble


@app.route('/sentiment')
def sentiment():
    '''
    Route for the sentiment graph.
    '''
    if not g.filename:
        return redirect('/')

    custom = False
    # Get parameters
    #   Cutoff value
    try:
        cutoff = float(request.args.get('cutoff'))
        if cutoff != 0.2:
            custom = True
    except (ValueError, TypeError):
        print("Error parsing cutoff slider")
        cutoff = g.cutoff

    #   Lemmatized bool
    try:
        lemmatized = request.args.get('lemmatized').lower() == 'true'
    except (ValueError, TypeError):
        print("Error parsing empath checkbox")
        lemmatized = False

    #   Cutoff value
    try:
        empath = request.args.get('empath').lower() == 'true'
        if(empath and lemmatized):
            custom = True
    except (ValueError, TypeError):
        print("Error parsing lemmatized checkbox")
        empath = False

    print("-------\nlemmatized: {}\nempath: {}\ncutoff: {}\n------".format(lemmatized,
                                                                           empath, cutoff))

    if not custom:
        # Check if file has been changed; then check if something new
        #  has been made for this specific set of parameters.
        # If cutoff or file hasn't been changed, use the
        # pre-generated files.
        if not lemmatized and not empath:
            if not g.sentiment:
                print('generating new diagram for new dataset')
                sentiment_html_gen.generate_sentiment_data(
                    g.filename, './visualisation_flask/templates/generated/sentiment_output.html',
                    empath=empath,
                    lemmatized=lemmatized)
            result = render_template('sentiment.html',
                                     filename='./generated/sentiment_output.html',
                                     name='Sentiment Analysis Scatterplot',
                                     cutoff=cutoff,
                                     subtitle='Default')
            session['sentiment'] = True

        elif empath and not lemmatized:
            if not g.sentiment_empath:
                print('generating new empath diagram for new dataset')
                sentiment_html_gen.generate_sentiment_data(
                    g.filename,
                    './visualisation_flask/templates/generated/sentiment_output_empath.html',
                    empath=True)
            result = render_template('sentiment.html',
                                     filename='./generated/sentiment_output_empath.html',
                                     name='Sentiment Analysis Scatterplot',
                                     cutoff=cutoff,
                                     subtitle='Grouped using Empath')
            session['sentiment_empath'] = True

        elif lemmatized and not empath:
            if not g.sentiment_lemmatized:
                print('generating new lemmatized diagram for new dataset')
                path = './visualisation_flask/templates/generated/sentiment_output_lemmatized.html'
                sentiment_html_gen.generate_sentiment_data(g.filename,
                                                           path,
                                                           lemmatized=True)
            result = render_template('sentiment.html',
                                     filename='./generated/sentiment_output_lemmatized.html',
                                     name='Sentiment Analysis Scatterplot',
                                     cutoff=cutoff,
                                     subtitle='Lemmatized and Stopwords Removed')
            session['sentiment_lemmatized'] = True

        else:
            # If none of these combinations are chosen, generate a custom diagram
            custom = True

    # If parameters don't have a diagram already generated, make one
    if custom:
        print('Generating custom diagram')
        sentiment_html_gen.generate_sentiment_data(
            g.filename,
            './visualisation_flask/templates/generated/sentiment_custom_output.html',
            empath=empath,
            lemmatized=lemmatized,
            cutoff=cutoff)
        session['sentiment'] = True
        result = render_template('sentiment.html',
                                 filename='./generated/sentiment_custom_output.html',
                                 name='Custom Sentiment Analysis Scatterplot',
                                 cutoff=cutoff,
                                 subtitle='Empath: {}, Lemmatized: {}, Cutoff: {}'
                                 .format(empath, lemmatized, cutoff))

    session['cutoff'] = cutoff
    return result

@app.route('/fishbone')
def fishbone():
    '''
    Route for the fishbone.
    '''
    if not g.filename:
        return redirect('/')
    # get num topics
    try:
        num_topics = int(request.args.get('num_topics'))
    except (ValueError, TypeError):
        num_topics = g.num_topics
    # first, we parse our data file and generate the dataset if it hasnt been done
    if not g.fishbone or num_topics != g.num_topics:
        path = './visualisation_flask/static/data/fishbone_lda.json'
        # special case for testing
        if current_app.config['TESTING']:
            path = '../visualisation_flask/static/data/fishbone_lda.json'
            g.filename = '../visualisation_flask/upload_folder/full_set_01.csv'
        fishbone_data_gen.generate_fishbone_data(
            g.filename, path, num_topics=num_topics)
        session['fishbone'] = True
        session['topics'] = num_topics
    # grab topic from query string
    topic = request.args.get('topic')
    if topic is None:
        topic = 'main'
    print(',' in topic, topic)
    # no spaces and commas means its a subtopic
    if ' ' not in topic and ',' in topic:
        documents = []
        uppertopic = ''
        path = './visualisation_flask/static/data/fishbone_lda.json'
        if current_app.config['TESTING']:
            path = '../visualisation_flask/static/data/fishbone_lda.json'
        with open(path) as f:
            fish = json.load(f)
            found = False
            for t in fish["children"]:
                for subt in t["children"]:
                    if subt["name"] == topic:
                        documents = subt["children"]
                        uppertopic = t["name"]
                        found = True
                        break
                if found:
                    break
        documents = [d["name"] for d in documents]
        print('fddd')
        return render_template('fishbone_list.html',
                               documents=documents,
                               uppertopic=uppertopic,
                               topic=topic)
    return render_template('fishbone.html', topic=topic)


@app.route('/keywords')
def keywords():
    '''
    Route for the keyword graph
    '''
    if not g.filename:
        return redirect('/')
    # get number of articles to be displayed
    try:
        num_articles = int(request.args.get('num_articles'))
    except (ValueError, TypeError):
        num_articles = g.num_articles
    # First, generate the keyword HTML file if it hasnt been done
    if not g.keywords or num_articles != g.num_articles:
        path = './visualisation_flask/templates/generated/networkx.html'
        keyword_graph_gen.generate_keyword_graph(g.filename, path, top_k=num_articles)
        session['keywords'] = True
        session['articles'] = num_articles

    return render_template('keyword.html',
                           filename='./generated/networkx.html',
                           name='Common Keyword Graph')


@app.route('/bubble')
def bubble():
    '''
    Route for the bubble chart
    '''
    if not g.filename:
        return redirect('/')
    # First, generate the required csv and json files if they havent been created
    if not g.bubble:
        frequency_path = './visualisation_flask/static/data/keyword_frequency.csv'
        keyword_to_title_path = './visualisation_flask/static/data/keyword_to_titles.json'
        bubble_data_gen.get_keyword_frequency(g.filename, frequency_path)
        bubble_data_gen.get_keyword_to_title(g.filename, keyword_to_title_path)
        session['bubble'] = True
    # Then grab target keyword from the bubble graph javascript if there is any.
    target = request.args.get('keyword')
    # This condition checks if the sub-graph is asked to be created
    if target is not None:
        keyword_to_title_path = './visualisation_flask/static/data/keyword_to_titles.json'
        subgraph_data_path = './visualisation_flask/static/data/subgraph_data.json'
        bubble_data_gen.generate_subgraph_data(target, keyword_to_title_path, subgraph_data_path)
        return render_template('bubble_sub_graph.html', name=target)
    # Or generate the main bubble graph
    return render_template('bubble.html', name='Bubble Chart')

# tree, which uses the same data source as the fishbone
@app.route('/tree')
def tree():
    '''
    Route for the tree. This uses the same datasource as the fishbone.
    '''
    if not g.filename:
        return redirect('/')
    # get num topics
    try:
        num_topics = int(request.args.get('num_topics'))
    except (ValueError, TypeError):
        num_topics = 20
    # first, we parse our data file and generate the dataset if it hasnt been done
    if not g.fishbone or num_topics != g.num_topics:
        path = './visualisation_flask/static/data/fishbone_lda.json'
        fishbone_data_gen.generate_fishbone_data(
            g.filename, path, num_topics=num_topics)
        session['fishbone'] = True
        session['topics'] = num_topics
    return render_template('tree.html')


@app.route('/neo4j')
def neo4j():
    '''
    Route for the Neo4J install instructions
    '''
    return render_template('neo4j.html')


@app.route('/neo4j-download')
def neo4j_download():
    '''
    Route for download the Neo4J database
    '''
    return send_file('templates/generated/full-graph.graphml',
                     as_attachment=True,
                     attachment_filename="full-graph.graphml")

@app.route('/ldavis')
def ldavis():
    '''
    Route for ldavis
    '''
    if not g.filename:
        return redirect('/')
    # get num topics
    try:
        num_topics = int(request.args.get('num_topics'))
    except (ValueError, TypeError):
        num_topics = 20
    # first, we parse our data file and generate the dataset if it hasnt been done
    if not g.ldavis or num_topics != g.lda_num_topics:
        ldavis_gen.generate_lda_vis(g.filename,
                                    './visualisation_flask/templates/generated/lda.html',
                                    num_topics=num_topics)
        session['ldavis'] = True
        session['lda_topics'] = num_topics
    return render_template('lda.html', name="ldavis", filename='./generated/lda.html')


@app.route('/file', methods=['GET', 'POST'])
def file_page():
    '''
    Route for the file uploader
    '''
    if request.method == 'POST':
        if 'file' not in request.files:
            print('post incorrect')
            return render_template('index.html')

        file = request.files['file']

        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            print('file not chosen when uploading')
            return render_template('file.html')

        # Otherwise add file and use
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(
                current_app.config['UPLOAD_FOLDER'], filename))
            session['filename'] = os.path.join(
                current_app.config['UPLOAD_FOLDER'], filename)

        # new file has been uploaded, tell sections to redo analysis
        session['fishbone'] = False
        session['topics'] = 20
        session['sentiment'] = False
        session['sentiment_empath'] = False
        session['sentiment_lemmatized'] = False
        session['cutoff'] = 0.2
        session['keywords'] = False
        session['bubble'] = False
        session['articles'] = 10
        return redirect('/')

    return render_template('file.html')


@app.route('/')
def index():
    '''
    Route for the home page
    '''
    g.filename = default_filename
    return render_template('index.html',
                           topic_num=g.num_topics,
                           article_num=g.num_articles,
                           cutoff=g.cutoff)
