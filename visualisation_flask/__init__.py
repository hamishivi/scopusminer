'''
The main launch point for the app. Place code in here to run on startup.
'''
import os
import nltk
from flask import Flask

UPLOAD_FOLDER = 'visualisation_flask/upload_folder'


def create_app():
    """Create and configure an instance of the Flask application."""
    app = Flask(__name__)
    app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
    app.secret_key = 'SECRET'
    # place your nltk downloads here
    nltk.download('stopwords')
    nltk.download('wordnet')

    from . import views
    app.register_blueprint(views.app)
    return app
