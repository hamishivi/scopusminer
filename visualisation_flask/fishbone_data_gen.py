'''
The module for generating LDA analysis, used
for the fishbone and tidy tree visualisations.
'''
import re
import json
import os
from operator import itemgetter

import pandas as pd
# Gensim
import gensim
import gensim.corpora as corpora # pylint: disable=C0414
from gensim.utils import simple_preprocess
# spacy for lemmatization
import spacy

# do our setup and cleaning
def generate_fishbone_data(filename, output_file, num_topics=20):
    '''
    Entry point for script.
    Num topics is the amount of groups LDA tries to split things into.
    '''
    if not os.path.exists(filename):
        print('could not find', filename)
        return
    df = pd.read_csv(filename)
    df = df.dropna()
    df['abstract'] = df['abstract'].astype(str)
    df['title'] = df['title'].astype(str)
    df['subjects'] = df['subjects'].apply(lambda x: re.sub(r'[^A-Za-z,\- ]', '', x).split(', '))
    df['abstract'] = df['abstract'].apply(lambda x: re.sub(r'[\xc2\xa9][^\.]*\.', ' ', x))

    data_words = df.abstract.values.tolist()

    # NLTK Stop words setup
    from nltk.corpus import stopwords
    stop_words = stopwords.words('english')
    stop_words.extend(['from', 'subject', 're', 'edu', 'use'])

    def sent_to_words(sentences):
        for sentence in sentences:
            yield gensim.utils.simple_preprocess(str(sentence), deacc=True)

    # Build the bigram and trigram models
    bigram = gensim.models.Phrases(data_words, min_count=5, threshold=100)

    # Faster way to get a sentence clubbed as a trigram/bigram
    bigram_mod = gensim.models.phrases.Phraser(bigram)

    # Define functions for stopwords, bigrams, trigrams and lemmatization
    def remove_stopwords(texts):
        return [[word for word in simple_preprocess(str(doc)) if word not in stop_words] \
                 for doc in texts]

    def make_bigrams(texts):
        return [bigram_mod[doc] for doc in texts]

    def lemmatization(texts, allowed_postags=['NOUN', 'ADJ', 'VERB', 'ADV']):
        """see https://spacy.io/api/annotation"""
        texts_out = []
        for sent in texts:
            doc = nlp(" ".join(sent))
            texts_out.append([token.lemma_ for token in doc if token.pos_ in allowed_postags])
        return texts_out

    # Initialize spacy 'en' model, keeping only tagger component (for efficiency)
    # python3 -m spacy download en
    nlp = spacy.load('en', disable=['parser', 'ner'])


    # now we create the JSON objects that the fishbone JS code wants.
    main_fishbone = {"name": "Scopus", "children": []}
    fishbones = []
    subjects_seen = {}

    # group by subject. Can this be optimised?
    for row in df.itertuples():
        for subject in row.subjects:
            if subject not in subjects_seen:
                subjects_seen[subject] = []
            subjects_seen[subject].append((row.title, row.abstract))

    for subject in subjects_seen:
        print('fitting topic', subject, "-- titles:", len(subjects_seen[subject]))
        # grab data
        titles = [r[0] for r in subjects_seen[subject]]
        abstracts = [r[1] for r in subjects_seen[subject]]
        data_words = list(sent_to_words(abstracts))
        # clean text
        data_words_nostops = remove_stopwords(data_words)
        data_words_bigrams = make_bigrams(data_words_nostops)
        data_lemmatized = lemmatization(data_words_bigrams, \
                                        allowed_postags=['NOUN', 'ADJ', 'VERB', 'ADV'])
        id2word = corpora.Dictionary(data_lemmatized)
        texts = data_lemmatized
        corpus = [id2word.doc2bow(text) for text in texts]
        # make model!
        lda_model = gensim.models.ldamodel.LdaModel(corpus=corpus,
                                                    id2word=id2word,
                                                    num_topics=num_topics,
                                                    random_state=100,
                                                    update_every=1,
                                                    chunksize=100,
                                                    minimum_probability=0,
                                                    passes=10,
                                                    alpha='auto',
                                                    per_word_topics=True)
        # filter documents into topics
        topic_groups = [[] for _ in range(num_topics)]
        topic_names = [",".join([w[0] for w in lda_model.show_topic(x, 5)]) \
                       for x in range(num_topics)]
        children = []
        cnt = 0
        for doc in corpus:
            doc_topics, _, _ = lda_model.get_document_topics(doc, per_word_topics=True)
            max_topic_val = doc_topics[0][1]
            max_topic_ind = doc_topics[0][0]
            for topic in doc_topics:
                if topic[1] > max_topic_val:
                    max_topic_val = topic[1]
                    max_topic_ind = topic[0]
            topic_groups[int(max_topic_ind)].append((max_topic_val, titles[cnt]))
            cnt += 1
        for k, g in enumerate(topic_groups):
            # sort so we can only get the top documents later
            top_g = sorted(g, key=itemgetter(0), reverse=True)
            if len(top_g) > 0: #pylint: disable=C1801
                children.append({"name": topic_names[k], "children": [{"name": s[1]} \
                                                                      for s in top_g]})
        fishbones.append({"name": subject, "children": children})

    # put it all together and make the file
    main_fishbone["children"] = fishbones
    str_bone = json.dumps(main_fishbone)
    with open(output_file, 'w') as f:
        f.write(str_bone)
