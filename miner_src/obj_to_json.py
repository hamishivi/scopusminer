'''this module converts a python object to json string'''
import csv
import jsonpickle

def to_json(obj):
    """converts object to string in json format"""
    return jsonpickle.encode(obj)

# this is kinda a meh implementation, but it works for simple stuff
def to_csv(obj_list, out_name):
    """converts python dictionaries to a csv. assumes all objects in the
    list will have the same set of keys. Make sure you convert our objects
    to dictionaries before passing into here!"""

    if not obj_list:
        return

    with open(out_name, 'w') as f:
        wr = csv.writer(f, quoting=csv.QUOTE_ALL)
        # grab first item and its key names
        wr.writerow(obj_list[0].keys())
        for obj in obj_list:
            attrs = []
            for key in obj:
                attrs.append(obj[key])
            wr.writerow(attrs)
