"This module handles parsing data from scopus and retreiving extra parts if necessary"
from datetime import datetime
from requests import HTTPError
from elsapy.elsdoc import AbsDoc
from miner_src.preprocess import api_setup, get_objects_from_elsapy

CACHE_DICT = dict()

def make_api_call(client, url):
    "Make the api call, using the cached result if possible"
    if url in CACHE_DICT:
        print("using cache")
        return CACHE_DICT[url]
    response = client.exec_request(url)
    CACHE_DICT[url] = response
    return response

class PaperData:
    "'Datastore for the paper; authors/citations/references are stored in lists"

    # pylint: disable=too-many-instance-attributes,too-few-public-methods
    # Datastore class, this many instance variables and few methods is
    # reasonable as data is only accessed/modified.

    def __init__(self, paper_id=0):
        # General paper information
        self.paper_id = paper_id                              # number
        self.title = ""                                       # text
        self.num_citations = None                             # number
        self.num_downloads = 0                             # number
        self.publication_date = None                          # datetime
        self.journal_name = None                              # text
        self.journal_impact_factor = None                     # number
        self.abstract = None                                  # text
        self.subjects = []

        # List of PaperAuthor objects
        self.authors = []
        # dict of citation IDs / titles. Perhaps list of tuples for ordering?
        self.citations = []
        # dict of reference IDs / titles. Perhaps list of tuples for ordering?
        self.references = []

    def to_dict(self):
        "Convert the object to a dictionary"
        d = dict()
        d["paper_id"] = self.paper_id
        d["title"] = self.title
        d["num_citations"] = self.num_citations
        d["num_downloads"] = self.num_downloads
        d["publication_date"] = self.publication_date
        d["journal_name"] = self.journal_name
        d["journal_impact_factor"] = self.journal_impact_factor
        d["abstract"] = self.abstract
        d["authors"] = self.authors
        d["citations"] = self.citations
        d["references"] = self.references
        d["subjects"] = self.subjects
        return d


class PaperAuthor:
    "'Another datastore for the author; there are multiple authors to each paper, and \
    the same author can occur multiple times (hence the designation 'paper author'"

    # pylint: disable=too-few-public-methods
    def __init__(self, name, sequence_number, citations):
        self.sequence_number = sequence_number
        self.author_name = name
        self.author_citations = citations


def get_elsadoc(client, scopus_id):
    "'Based on elsa documentation; Initialize document with Scopus ID."
    scp_doc = AbsDoc(scp_id=scopus_id)
    # print all the fields
    if scp_doc.read(client):
        print("Read document succeeded")
        # from pprint import pprint
        # pprint(vars(scp_doc))
    else:
        print("Read document failed.")
    return scp_doc

def extract_fieldset(client, elsapy_dict):
    "' Extract paper_id, title, num_citations, publication_date, num_downloads, \
        journal_name, journal_impact_factor, abstract. Calls helpers for more \
        complex fields"

    new_paper = PaperData()
    try:
        new_paper_id = elsapy_dict["dc:identifier"].strip("SCOPUS_ID:")
    except:  # pylint: disable=W0702
        print("could not retrive the paper identifier")
        return None

    # fill basic fields
    new_paper.paper_id = int(new_paper_id)
    new_paper.title = elsapy_dict["dc:title"]
    new_paper.num_citations = int(elsapy_dict["citedby-count"])
    new_paper.publication_date = datetime.strptime(
        elsapy_dict["prism:coverDate"], '%Y-%m-%d')
    new_paper.journal_name = elsapy_dict["prism:publicationName"]
    new_paper.journal_impact_factor = extract_cite_score(client, elsapy_dict)
    new_paper.abstract = extract_abstract(client, elsapy_dict)
    new_paper.subjects = get_subjects(client, elsapy_dict)

    # fill more complex fields
    try:
        new_paper.authors = extract_authors(new_paper_id, client)
    except Exception as e:  # pylint: disable=W0703
        new_paper.authors = []
        print("There was an error in getting authors", e)
    try:
        new_paper.citations = extract_citations(
            new_paper_id, client, elsapy_dict)
    except Exception as e:  # pylint: disable=W0703
        new_paper.citations = []
        print("There was an error getting citations", e)
    try:
        new_paper.references = extract_references(
            new_paper_id, client, elsapy_dict)
    except Exception as e:  # pylint: disable=W0703
        new_paper.references = []
        print("There was an error in getting references", e)

    return new_paper


def get_subjects(client, elsapy_dict):
    "Extract subject areas of paper"
    eid = elsapy_dict["eid"]
    try:
        response = make_api_call(client, "https://api.elsevier.com/content/abstract/eid/"
                                 + eid + "?view=FULL")
    except HTTPError:
        return None
    subjects = []
    try:
        subj_list = response["abstracts-retrieval-response"]["subject-areas"]["subject-area"]
        for subj in subj_list:
            subjects.append(subj["$"])
    except Exception as e: # pylint: disable=W0703
        print("There was an error in getting subjects", e)
    return subjects

def extract_abstract(client, elsapy_dict):
    "Extract the abstract of the paper"
    eid = elsapy_dict["eid"]
    try:
        response = make_api_call(client, "https://api.elsevier.com/content/abstract/eid/"
                                 + eid + "?view=META_ABS")
    except HTTPError:
        return None
    return response["abstracts-retrieval-response"]["coredata"]["dc:description"]


def extract_cite_score(client, elsapy_dict):
    "'Extract the cite score "
    issn = elsapy_dict.get("prism:issn", None)
    if issn is None:
        return 0.0
    try:
        response = make_api_call(client,
                                 "https://api.elsevier.com/content/serial/title/issn/" + issn)
    except HTTPError:
        print("Failed to find cite score")
        return 0.0

    # We assume the first item in the result list is the title we are looking for.
    # if we got the issn, we know that we got the right one, but the api still
    # returns a list for some reason.
    try:
        SNIPnum = response["serial-metadata-response"]["entry"][0]['SNIPList']['SNIP'][0]["$"]
    except KeyError:
        return 0.0

    return float(SNIPnum)


def extract_authors(paper_id, client):
    "'Extract sequence_number, author_name, author_citations for a paper"
    response = make_api_call(client,
                             "http://api.elsevier.com/content/abstract/scopus_id/" + paper_id)
    authors = []
    # sometimes this is a list, sometimes it is a single item...
    author_group_list = response["abstracts-retrieval-response"]["item"]\
                                ["bibrecord"]["head"]["author-group"]

    if isinstance(author_group_list, list):
        for group in author_group_list:
            author_list = group["author"]
            for author in author_list:
                sequence_number = author["@seq"]
                author_name = author["preferred-name"]["ce:indexed-name"]
                # to get citations, we require another request
                response = make_api_call(client,
                                         "https://api.elsevier.com/content/author/author_id/"
                                         + author["@auid"])
                citations = response["author-retrieval-response"][0]["coredata"]["citation-count"]
                authors.append({"paper_id": paper_id, "sequence_number": sequence_number,
                                "name": author_name, "num_citations": citations})
    else:
        author_list = author_group_list["author"]
        for author in author_list:
            sequence_number = author["@seq"]
            author_name = author["preferred-name"]["ce:indexed-name"]
            # to get citations, we require another request
            response = make_api_call(client, "https://api.elsevier.com/content/author/author_id/"
                                     + author["@auid"])
            citations = response["author-retrieval-response"][0]["coredata"]["citation-count"]
            authors.append({"paper_id": paper_id, "sequence_number": sequence_number,
                            "name": author_name, "num_citations": citations})
    return authors


def extract_citations(paper_id, client, elsapy_dict):
    "Extract a list of citations (citation_ids, citation_titles) for a paper"
    response = make_api_call(client, "https://api.elsevier.com/content/search/scopus?query=refeid("
                             + str(elsapy_dict['eid']) + ')')
    citations = []
    for entry in response["search-results"]["entry"]:
        try:
            ref_id = entry["dc:identifier"].strip("SCOPUS_ID:")
            title = entry["dc:title"]
            citations.append({"paper_id": paper_id, "title": title, "id": ref_id})
        except:  # pylint: disable=W0702
            print("could not retrive the citation title")
            print(entry)
    return citations


def extract_references(paper_id, client, elsapy_dict):
    "Extract a list of references (reference_ids, reference_titles) for a \
      paper, reference_titles"
    response = make_api_call(client, 'https://api.elsevier.com/content/abstract/eid/' +
                             elsapy_dict["eid"] + "?view=REF")
    refs = []
    response_references = response['abstracts-retrieval-response']['references']['reference']
    for ref in response_references:
        ref_id = ref['scopus-id']
        is_paper = False
        response = make_api_call(client, "http://api.elsevier.com/content/abstract/scopus_id/"
                                 + ref_id)
        try:
            if "dc:title" not in response["abstracts-retrieval-response"]["coredata"]:
                if "sourcetitle" in response["abstracts-retrieval-response"]["item"]\
                                            ["bibrecord"]["head"]["source"]:
                    ref_title = str(response["abstracts-retrieval-response"]["item"]
                                    ["bibrecord"]["head"]["source"]["sourcetitle"])
                else:
                    ref_title = str(response["abstracts-retrieval-response"]["item"]
                                    ["bibrecord"]["head"]["source"]["bib-text"])
            else:
                ref_title = response["abstracts-retrieval-response"]["coredata"]["dc:title"]
                is_paper = True
        except:  # pylint: disable=W0702
            print("could not retrive the reference title")
            ref_title = 'ERROR: COULD NOT FIND'
        refs.append({"paper_id": paper_id, "ref_id": ref_id,
                     "ref_title": ref_title, "is_paper": is_paper})
    return refs


def main():
    "'Experimenting with Elsapy objects"
    titles = ['GHUMVEE - Efficient, effective, and flexible replication']
    client = api_setup()
    get_objects_from_elsapy(client, titles)


if __name__ == "__main__":
    main()
