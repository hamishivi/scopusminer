"'Preprocessing data and making initial API requests as needed."
import json
from multiprocessing import Pool
from functools import partial
import re
import xlrd
from elsapy.elsclient import ElsClient
from elsapy.elssearch import ElsSearch


def compare_two_titles(title1, title2):
    "'function to compare the input title with the retrieved title'"
    # Do this because the retrieved titles will have &amp; instead of &
    title1 = re.sub("&amp;", '', title1)
    title2 = re.sub("&amp;", '', title2)
    first = re.sub('[^A-Za-z0-9]+', '', title1).lower()
    second = re.sub('[^A-Za-z0-9]+', '', title2).lower()
    if first == second:
        return True
    return False


def title_preprocessing(title):
    "'Get rid of the invalid characters of the title'"
    # these are some URL reserved characters
    for ch in ['(', ')', '&', ':', '/', '?', '#', '[', ']', '@', '!', '$', '*', '+', ',', ';', '=']:
        if ch in title:
            title = title.replace(ch, ' ')
    return title


def get_all_titles(file_name):
    "'retrieve all titles from the input excel file'"
    work_book = xlrd.open_workbook(file_name)  # Load the excel workbook
    sheet = work_book.sheet_by_index(0)
    strlist = []
    for value in range(sheet.nrows):
        strlist.append(sheet.cell_value(value, 0))
    return strlist


def api_setup():
    "'Set up the api environment"
    # Load configuration
    con_file = open("config.json")
    config = json.load(con_file)
    con_file.close()
    # Initialize client
    client = ElsClient(config['apikey'])
    return client


def get_objects_from_elsapy(client, titles):
    "'Extract all objects from elsapy by the titles given in parallel.\n\
    Returns results, and an array of titles for which no result was found"
    # Run this with a pool of 5 agents having a chunksize of 3 until finished
    agents = 5
    chunksize = 3
    with Pool(processes=agents) as pool:
        results = pool.map(
            partial(search_one_object, client), titles, chunksize)

    missing = [titles[i] for i, item in enumerate(results) if not item]
    results = list(filter(lambda x: x, results))

    return results, missing


def search_one_object(client, title):
    "'Seach for a single object in Elsapy'"
    title = title_preprocessing(title)
    querystr = 'TITLE(' + title + ')'
    doc_srch = ElsSearch(querystr, 'scopus')
    doc_srch.execute(client, get_all=True)
    # this includes the case that no result is found
    if doc_srch.num_res == 1:
        result = doc_srch.results[0]
        return result
    # get the one with exactly the same title if more than one result are returned
    for res in doc_srch.results:
        if compare_two_titles(res["dc:title"], title):
            return res

    return None
