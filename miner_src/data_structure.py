"'Andrew Part"

class PaperData:
    """Class for a single paper, storing relevant data"""
    def __init__(self, paper_id, title="", num_citations=None, num_downloads=None, \
        publication_date=None, journal_name=None, journal_impact_factor=None, \
        abstract=None, authors=[None], citations={}, references={}):

        # General paper information

        self.id = paper_id                                  #pylint: disable=invalid-name
        self.title = title                                  # text
        self.num_citations = num_citations                  # number
        self.num_downloads = num_downloads                  # number
        self.publication_date = publication_date            # datetime
        self.journal_name = journal_name                    # text
        self.journal_impact_factor = journal_impact_factor  # number
        self.abstract = abstract                            # text

        # List of author objects;
        self.authors = authors
        # dict of citation IDs and titles. Could be list of tuples to maintain order.
        self.citations = citations
        # dict of reference IDs and titles. Could be list of tuples to maintain order.
        self.references = references

class PaperAuthor:
    "'Another datastore for the author; there are multiple authors to each paper, and \
    the same author can occur multiple times (hence the designation 'paper author'"
    def __init__(self, author_name, sequence_number, author_citations):
        self.sequence_number = sequence_number
        self.author_name = author_name
        self.author_citations = author_citations
